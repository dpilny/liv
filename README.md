# LocalInventoryWatcher
Organize your local inventory, keep track of what you have on your shelves and cupboards. 
LIW utilizes a barcode scanner to scan products and allows you to add or remove them from your inventory or add them your Bring shopping list.

## Configuration
The server and hmi applications are configured by providing a ".env"-configuration file.
### Server
The server applications connects to a MySQL-Database, hosts the ReactJS frontend and a API, handles input from a barcode scanner
```
dbUser=<Username for database access>
dbPassword=<Password for database access>
dbName=<Database name>
dbHost=<Hostname of database>

scannerActive=<If true activates barcode scanner handling>
scannerDeviceId=<deviceId for barcodescanner>

openGTINEnabled=<if true activates openGTIN EAN-product resolver>
openGTINAPIKey=<API Key for openGTIN API which is used for EAN code resolving>

serverPort=<Port which is used for frontend/api server>

bringEmail=<EMail for Bring authentication>
bringPassword=<Password for Bring authentication>

hmiServer=<Address for hmi server>
hmiServerPort=<Port for hmi server>
```

### HMI
The HMI application opens a gRPC-server which is used to receive display commands from the main application.
```
port=<Port which is used for hmi server>
```

#### Build 
The WS2811b library uses cgo to compile, so its not easily possible to cross compile from a non-arm system, thus a docker image is used to compile the application.
The Image file can be found at: ````hmi/Dockerfile````. Original [Source][1]

To compile the application used the following command:   
```
docker run --rm -ti -v "$(pwd)":/go/src/home rpi-ws281x-go-builder /bin/sh -c "go build -o /go/src/home/hmi/hmi -v /go/src/home/hmi/main.go"

docker run --rm -ti -v "$(pwd)":/go/src/home rpi-ws281x-go-builder /bin/sh -c "go build -o /go/src/home/hmi/grpc/client -v /go/src/home/hmi/grpc/client/main.go"

check file with 'file '
```

## Requirements
1. A MariaDB/MySQL server (see schema.sql for the db schema)
2. A unix-System which runs the server application (tested with Raspbian)
3. A RPi whichs runs the HMI application
### Hardware
LIW was developed to run on a RaspberryPi which has a WS2811b LED strip wired to it.  

## TODOs
- [ ] externalize scanner to standalone application - communicate between applications by gRPC (this would allow the server to run on any go supported system) 



[1]: https://github.com/rpi-ws281x/rpi-ws281x-go/blob/master/Dockerfile