package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/dpilny/liw/grpc"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	var address string
	//address := flag.String("address", "localhost:8080", "address of hmi_ grpc server")
	flag.StringVar(&address, "a", "localhost:8080", "address of hmi_ grpc server")
	flag.Parse()
	fmt.Println("starting cli cli with address:", address)
	ds, err := grpc.GetClient(address)
	if err != nil {
		log.Panic("failed to open grpc connection", err)
	}

	fmt.Println("got connection to grpc server")
	defer ds.Close()
	reader := bufio.NewReader(os.Stdin)
	var interval = 350
	var duration = 0
	for {
		fmt.Print("Enter valid rgb hex code to display (comma seperated for multi values): ")
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("got err, quitting application:", err)
			break
		}
		input = strings.ReplaceAll(input, "\n", "")
		if input == "q" {
			fmt.Println("quitting cli application")
			break
		}
		if strings.HasPrefix(input, "d") || strings.HasPrefix(input, "i") {
			values := strings.Split(input, "=")
			if len(values) < 2 {
				fmt.Println("specify new duration by separating th")
			}
			value, err := strconv.ParseInt(values[1], 10, 64)
			if err != nil {
				log.Println("failed to parse duration/interval value", err)
				continue
			}
			if strings.HasPrefix(input, "d") {
				duration = int(value)
				log.Println("set duration to:", duration)
			} else {
				interval = int(value)
				log.Println("set interval to:", interval)
			}
			continue
		}

		stringColors := strings.Split(input, ",")
		var colors []uint32
		for _, stringColor := range stringColors {
			stringColor = strings.Replace(stringColor, "0X", "", -1)
			n, err := strconv.ParseUint(stringColor, 16, 64)
			if err != nil {
				log.Println("invalid hex number:", n)
				log.Println(err)
				continue
			}
			colors = append(colors, uint32(n))
		}
		fmt.Println("displaying colors:", colors)
		ds.DisplayState(colors, int32(interval), int64(duration))
	}
}
