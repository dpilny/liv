package grpc

import (
	"context"
	"gitlab.com/dpilny/liw/grpc/hmi"
	"google.golang.org/grpc"
	"log"
)

type DisplayClient struct {
	client hmi.StateServiceClient
	conn   *grpc.ClientConn
}

func GetClient(address string) (*DisplayClient, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}
	client := hmi.NewStateServiceClient(conn)
	return &DisplayClient{client: client, conn: conn}, nil
}

func (ds *DisplayClient) DisplayState(colors []uint32, interval int32, duration int64) {
	request := &hmi.HMIColorRequest{
		Colors:   colors,
		Interval: interval,
		Duration: duration,
	}
	_, err := ds.client.DisplayColor(context.Background(), request)
	if err != nil {
		log.Println("failed to display state", err)
	}
}

func (ds *DisplayClient) Close() {
	err := ds.conn.Close()
	if err != nil {
		log.Println("failed to close grpc conn", err)
	}
}
