# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=liw
BINARY_RPI=$(BINARY_NAME)_rpi

all: test build
test:
		$(GOTEST) -v ./...
clean:
		$(GOCLEAN)
		rm -f $(BINARY_NAME)
		rm -f $(BINARY_UNIX)
		rm -f $(BINARY_RPI)

# Cross compile arm
build-backend-rpi:
		GOOS=linux GOARCH=arm $(GOBUILD) -o $(BINARY_RPI) -v backend/main.go

build-backend:
		$(GOBUILD) -o $(BINARY_NAME) -v backend/main.go

build-frontend:
		npm run --prefix frontend build

build-rpi-docker:
		docker build --tag rpi-ws281x-go-builder hmi

build-hmi-rpi:
		docker run --rm -ti -v "${CURDIR}":/go/src/gitlab.com/dpilny/liw rpi-ws281x-go-builder /bin/sh -c "go build -o /go/src/gitlab.com/dpilny/liw/hmi/hmi -v /go/src/gitlab.com/dpilny/liw/hmi/main.go"

move-hmi-to-pi:
		scp hmi/hmi pi@homeiot:/home/pi/liw

move-backend-to-pi:
		scp liw_rpi pi@homeiot:/home/pi/liw

clean-frontend:
		ssh pi@homeiot 'rm -rf /home/pi/liw/frontend/build'

move-frontend-to-pi:
		scp -r frontend/build pi@homeiot:/home/pi/liw/frontend

gen-hmi-grpc:
		protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative grpc/hmi/hmi.proto
