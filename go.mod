module gitlab.com/dpilny/liw

go 1.17

require (
	github.com/VividCortex/mysqlerr v0.0.0-20201215173831-4c396ae82aac
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/gddo v0.0.0-20200604155040-845892271f91
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.5 // indirect
	gitlab.com/dpilny/liw/grpc v0.0.0
	golang.org/x/text v0.3.4 // indirect
)

replace gitlab.com/dpilny/liw/grpc => ./grpc
