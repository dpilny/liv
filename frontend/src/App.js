import React, {useState} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom"
import './App.scss';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import Dashboard from "./pages/Dashboard";
import {AuthContext} from "./context/auth";
import Login from "./pages/Login";
import NavBar from "./components/NavBar";
import CategoriesContainer from "./pages/categories/CategoriesContainer";
import LogContainer from "./pages/log/LogContainer";
import InventoryContainer from "./pages/inventory/InventoryContainer";
import ProductsContainer from "./pages/products/ProductsContainer";
import LostNFoundContainer from "./pages/lostnfound/LostNFoundContainer";

function App() {
    const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    const [authTokens, setAuthTokens] = useState(existingTokens);

    const setTokens = (data) => {
        localStorage.setItem("tokens", JSON.stringify(data));
        setAuthTokens(data);
    }
    return (
        <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens}}>
            <Router>
                <NavBar/>
                <div className="container bg-light pt-2 pb-3">
                    <Route exact path="/" component={Dashboard}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/inventory" component={InventoryContainer}/>
                    <Route path="/products" component={ProductsContainer}/>
                    <Route path="/categories" component={CategoriesContainer}/>
                    <Route path="/lostnfound" component={LostNFoundContainer}/>
                    <Route path="/log" component={LogContainer}/>
                </div>
            </Router>
        </AuthContext.Provider>
    );
}

export default App;
