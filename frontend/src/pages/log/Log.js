import React from "react";
import DisplayTable from "../../components/DisplayTable";

export default function Log(props) {

    const actionBody = (logEntry) => {
        if (logEntry.action === "add") {
            return (
                <span className={"pi pi-plus"}/>
            )
        } else {
            return (
                <span className={"pi pi-minus"}/>
            )
        }
    }

    const timestampBody = (timestamp) => {
        return <span>{new Date(timestamp.timestamp).toLocaleString()}</span>
    }

    const columns = [
        {
            header: "Aktion",
            field: "action",
            body: actionBody,
        },
        {
            header: "Beschreibung",
            field: "product_name",
            sortable: true
        },
        {
            header: "Anzahl",
            field: "amount",
            sortable: true
        },
        {
            header: "Zeitpunkt",
            field: "timestamp",
            sortable: true,
            body: timestampBody
        }
    ]

    return (
        <React.Fragment>
            <h3>History</h3>
            <DisplayTable data={props.logEntries} columns={columns} editable={false}/>
        </React.Fragment>
    )

}