import React, {useEffect, useState} from "react";
import {getHistoryLog} from "../../services/api";
import ContentPage from "../../components/ContentPage";
import Log from "./Log";


export default function LogContainer() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [logEntries, setLogEntries] = useState([]);

    useEffect(() => {
        getHistoryLog().then((logEntries) => {
                setLogEntries(logEntries.entries);
                setIsLoaded(true);
            },
            (error) => {
                setError(error);
                setIsLoaded(true);
            })
    }, [])

    return (
        <ContentPage error={error} isLoaded={isLoaded}>
            <Log logEntries={logEntries}/>
        </ContentPage>
    )

}