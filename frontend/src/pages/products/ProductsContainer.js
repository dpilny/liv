import React, {useEffect, useState} from "react";
import {getProductCategories, getProducts, updateProduct} from "../../services/api";
import ContentPage from "../../components/ContentPage";
import Products from "./Products";

export default function ProductsContainer() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [products, setProducts] = useState([]);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        getProducts().then(
            (products) => {
                setProducts(products.entries);
                getProductCategories().then(
                    (categories) => {
                        setCategories(categories.entries)
                        setIsLoaded(true)
                    },
                    (error) => {
                        setIsLoaded(true)
                        setError(error)
                    }
                )
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
    }, [])

    const updateItem = (id, payload) => {
        updateProduct(id, payload).then(
            (response) => {
                console.log("updated product");
            },
            (error) => {
                console.log("failed to update product");
                setIsLoaded(true);
                setError(error);
            })
    }

    return (
        <ContentPage error={error} isLoaded={isLoaded}>
            <Products products={products} setProducts={setProducts} categories={categories} updateItem={updateItem}/>
        </ContentPage>
    )

}