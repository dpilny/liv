import React, {useRef} from "react";
import DisplayTable from "../../components/DisplayTable";
import {Toast} from "primereact/toast";

export default function Products(props) {

    const toast = useRef(null)

    const updateItem = (oldItem, newItem) => {
        const categories = props.categories;
        let categoryId;
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].name === newItem['product_category']) {
                categoryId = categories[i].id;
            }
        }
        const payload = {
            product_name: newItem.name,
            product_description: newItem.description,
            product_category: categoryId,
            product_code: newItem.code
        }
        props.updateItem(newItem.id, payload)
        const msg = "Produkt '" + newItem.name + "' wurde aktualisiert."
        toast.current.show({severity: 'success', summary: 'Erfolg', detail: msg})
    }

    const columns = [
        {
            field: "name",
            header: "Name",
            editable: true,
            type: "string",
            sortable: true
        },
        {
            field: "description",
            header: "Beschreibung",
            editable: true,
            type: "string",
            sortable: true
        },
        {
            field: "product_category",
            header: "Kategorie",
            editable: true,
            type: "dropdown",
            options: props.categories,
            sortable: true
        },
        {
            field: "code",
            header: "EAN",
            editable: true,
            hideMobile: true,
            type: "string",
            sortable: true
        },
    ]

    return (
        <>
            <h3>Produkte</h3>
            <DisplayTable data={props.products} columns={columns} editable={true} updateData={props.setProducts}
                          updateItem={updateItem}/>
            <Toast ref={toast} baseZIndex={2000}/>
        </>
    )
}