import React from "react";
import {Button, Card, Form, Input} from '../components/AuthForm';

function Login() {
    return (
        <Card>
            <Form>
                <Input type="email" placeholder="email" />
                <Input type="password" placeholder="password" />
                <Button>Login</Button>
            </Form>
        </Card>
    );
}

export default Login;