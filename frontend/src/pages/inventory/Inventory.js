import React from "react";
import DisplayTable from "../../components/DisplayTable";

function Inventory(props) {

    const updateItem = (previousItem, newItem) => {
        const locations = props.locations;
        let newLocationId;
        let oldLocationId;
        for (let i = 0; i < locations.length; i++) {
            if (previousItem.location === locations[i].name) {
                oldLocationId = locations[i].id;
            }
            if (newItem.location === locations[i].name) {
                newLocationId = locations[i].id
            }
        }
        const payload = {
            new_amount: newItem.amount,
            new_location_id: newLocationId,
            location_id: oldLocationId
        }
        props.updateEntry(newItem.id, payload)
    }

    const columns = [
        {
            field: "name",
            header: "Name",
            sortable: true,
        },
        {
            field: "location",
            header: "Ort",
            editable: true,
            type: "dropdown",
            options: props.locations,
            sortable: true,
        },
        {
            field: "amount",
            header: "Anzahl",
            editable: true,
            type: "number",
            sortable: true,
        }
    ]

    return (
        <React.Fragment>
            <h3>Inventar</h3>
            <DisplayTable data={props.items} columns={columns} editable={true} updateData={props.setItems} updateItem={updateItem}/>
        </React.Fragment>
    )
}

export default Inventory;