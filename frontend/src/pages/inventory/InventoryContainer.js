import React, {useEffect, useState} from "react";
import {getInventory, getLocations, updateInventoryEntry} from "../../services/api";
import ContentPage from "../../components/ContentPage";
import Inventory from "./Inventory";

export default function InventoryContainer() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        getInventory().then(
            (result) => {
                setItems(result.entries);
                getLocations().then(
                    (locations) => {
                        setLocations(locations.entries)
                        setIsLoaded(true)
                    },
                    (error) => {
                        setIsLoaded(true)
                        setError(error)
                    }
                )
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
    }, [])

    const updateEntry = (itemId, payload) => {
        updateInventoryEntry(itemId, payload).then().catch(error => {
            setError(error)
        })
    }


    return (
        <ContentPage error={error} isLoaded={isLoaded}>
            <Inventory items={items} locations={locations} setItems={setItems} updateEntry={updateEntry}/>
        </ContentPage>
    )

}