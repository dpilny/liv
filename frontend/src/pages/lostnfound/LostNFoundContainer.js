import React, {useEffect, useState} from "react";
import LostNFound from "./LostNFound";
import ContentPage from "../../components/ContentPage";
import {createProductCategory, getProductCategories, getUnknownProducts, updateProduct} from "../../services/api";

export default function LostNFoundContainer() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    const [categories, setCategories] = useState([]);
    const [unknownProducts, setUnknownProducts] = useState([])

    const fetchUnknownProducts = () => {
        getUnknownProducts().then(
            (products) => {
                setUnknownProducts(products.entries);
                setIsLoaded(true)
            },
            (error) => {
                setError(error)
                setIsLoaded(true)
            }
        )
    }

    const updateItem = (id, payload) => {
        updateProduct(id, payload).then(
            (response) => {
                console.log("updated product");
                fetchUnknownProducts()
            },
            (error) => {
                console.log("failed to update product");
                setIsLoaded(true);
                setError(error);
            })
    }

    const createCategory = (payload) => {
        createProductCategory(payload.category_name, payload.inventory_code).then((response) => {
                fetchCategories();
            },
            (error) => {
                setIsLoaded(true);
                setError(error);
            })
    }

    const fetchCategories = () => {
        getProductCategories().then(
            (categories) => {
                setCategories(categories.entries)
                fetchUnknownProducts()
            },
            (error) => {
                setIsLoaded(true)
                setError(error)
            }
        )
    }

    useEffect(() => {
        fetchCategories()
    }, [])

    return (
        <ContentPage error={error} isLoaded={isLoaded}>
            <LostNFound unknownProducts={unknownProducts} categories={categories} updateProduct={updateItem}
                        createCategory={createCategory} setUnknownProducts={setUnknownProducts}/>
        </ContentPage>
    )

}