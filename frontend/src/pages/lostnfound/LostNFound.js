import React, {useRef} from "react";
import DisplayTable from "../../components/DisplayTable";
import CategoryCreator from "../categories/CategoryCreator";
import {Toast} from "primereact/toast";

export default function LostNFound(props) {

    const toast = useRef(null)

    const createCategory = (data) => {
        props.createCategory(data);
        let msg = "Kategorie '" + data.category_name + "'";
        if (data.inventory_code) {
            msg += " mit dem Code: '" + data.inventory_code + "'"
        }
        msg += " wurde erstellt!"
        toast.current.show({severity: 'success', summary: 'Erfolg', detail: msg});
    }

    const updateProduct = (oldItem, newItem) => {
        const categories = props.categories;
        let categoryId;
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].name === newItem['product_category']) {
                categoryId = categories[i].id;
            }
        }
        const payload = {
            product_name: newItem.name,
            product_description: newItem.description,
            product_category: categoryId,
            product_code: newItem.code
        }
        props.updateProduct(newItem.id, payload)
        const msg = "Produkt '" + newItem.name + "' wurde aktualisiert."
        toast.current.show({severity: 'success', summary: 'Erfolg', detail: msg})
    }

    const timestampBody = (entry) => {
        return <span>{new Date(entry['last_update']).toLocaleString()}</span>
    }

    const columns = [
        {
            field: "name",
            header: "Name",
            type: "string",
            editable: true,
            sortable: true
        },
        {
            field: "description",
            header: "Description",
            type: "string",
            editable: true,
            sortable: true
        },
        {
            field: "product_category",
            header: "Kategorie",
            editable: true,
            type: "dropdown",
            options: props.categories,
            sortable: true
        },
        {
            field: "last_update",
            header: "Gescanned am",
            sortable: true,
            body: timestampBody
        }
    ]

    return (
        <>
            <div className="p-d-flex">
                <h3>Fundgrube</h3>
                <CategoryCreator createCategory={createCategory}/>
            </div>
            <DisplayTable data={props.unknownProducts} columns={columns} updateItem={updateProduct}
                          updateData={props.setUnknownProducts} editable={true}/>
            <Toast ref={toast} baseZIndex={2000}/>
        </>
    )
}
