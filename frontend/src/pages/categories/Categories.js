import React, {useRef} from "react";
import DisplayTable from "../../components/DisplayTable";
import CategoryCreator from "./CategoryCreator";
import {Toast} from "primereact/toast";

function Categories(props) {

    const toast = useRef(null)

    const updateItem = (oldItem, newItem) => {
        const payload = {
            category_name: newItem.name,
            inventory_code: newItem["inventory_code"],
        }
        props.updateItem(newItem.id, payload)
    }

    const createCategory = (data) => {
        props.createItem(data);
        let msg = "Kategorie '" + data.category_name + "'";
        if (data.inventory_code) {
            msg += " mit dem Code: '" + data.inventory_code + "'"
        }
        msg += " wurde erstellt!"
        toast.current.show({severity: 'success', summary: 'Erfolg', detail: msg});
    }

    const getTagsForCategory = (category) => {
        if (!category['tag_ids']) {
            return [];
        }
        let tags = [];
        const tagIds = category['tag_ids'];
        for (let i = 0; i < tagIds.length; i++) {
            for (let j = 0; j < props.categoryTags.length; j++) {
                const tag = props.categoryTags[j];
                if (tagIds[i] === tag.id) {
                    tags.push(tag)
                }
            }
        }
        return tags;
    }

    const onTagsChanged = (element, values) => {
        console.log("data updated for " + JSON.stringify(element));
        console.log(values);

        const payload = {
            category_name: element.name,
            inventory_code: element["inventory_code"],
            tags: values
        }
        props.updateItem(element.id, payload);
    }

    const columns = [
        {
            field: "id",
            header: "Id",
            sortable: true
        },
        {
            field: "name",
            header: "Name",
            editable: true,
            type: "string",
            sortable: true
        },
        {
            field: "inventory_code",
            header: "Inventory Code",
            editable: true,
            type: "string",
            sortable: true
        },
        {
            field: "tag_ids",
            header: "Tags",
            editable: true,
            type: "chips",
            chipValuesFunc: getTagsForCategory,
            suggestions: props.categoryTags,
            onChange: onTagsChanged
        }
    ]

    return (
        <>
            <div className="p-d-flex">
                <h3>Produktkategorien</h3>
                <CategoryCreator createCategory={createCategory}/>
            </div>
            <DisplayTable data={props.categories} columns={columns} editable={true} updateData={props.setCategories}
                          updateItem={updateItem}/>
            <Toast baseZIndex={2000} ref={toast}/>
        </>
    )
}

export default Categories;
