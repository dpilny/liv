import ContentPage from "../../components/ContentPage";
import Categories from "./Categories";
import React, {useEffect, useState} from "react";
import {
    createProductCategory,
    getProductCategories,
    getProductCategoryTags,
    updateProductCategory
} from "../../services/api";

export default function CategoriesContainer() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    const [categories, setCategories] = useState([]);
    const [categoryTags, setCategoryTags] = useState([]);

    const fetchCategories = () => {
        getProductCategoryTags().then(
            (result) => {
                setCategoryTags(result.entries)
                getProductCategories().then(
                    (result) => {
                        console.log("got Result")
                        console.log(JSON.stringify(result))
                        setCategories(result.entries)
                        setIsLoaded(true)
                    },
                    (error) => {
                        setError(error)
                        setIsLoaded(true)
                    }
                )
            },
            (error) => {
                setError(error)
                setIsLoaded(true)
            }
        )
    }

    useEffect(() => {
        fetchCategories()
    }, [])

    const updateItem = (id, payload) => {
        updateProductCategory(id, payload).then(
            (response) => {
                console.log("updated category");
                fetchCategories()
            },
            (error) => {
                console.log("failed to update product");
                setIsLoaded(true);
                setError(error);
            })
    }

    const createItem = (payload) => {
        createProductCategory(payload.category_name, payload.inventory_code).then((response) => {
                fetchCategories();
            },
            (error) => {
                setIsLoaded(true);
                setError(error);
            })
    }

    return (
        <ContentPage error={error} isLoaded={isLoaded}>
            <Categories categories={categories} setCategories={setCategories} updateItem={updateItem}
                        createItem={createItem} categoryTags={categoryTags}/>
        </ContentPage>
    )

}