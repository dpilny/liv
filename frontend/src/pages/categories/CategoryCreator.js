import {InputText} from "primereact/inputtext";
import React, {useState} from "react";
import PropTypes from 'prop-types';
import {Dialog} from "primereact/dialog";

export default function CategoryCreator(props) {

    const [visible, setVisible] = useState(false)

    const [name, setName] = useState("");
    const [inventoryCode, setInventoryCode] = useState("");

    const [validInput, setValidInput] = useState(true)

    let inputClass = "";
    if (!validInput) {
        inputClass = "p-invalid"
    }

    const saveClicked = () => {
        if (!name) {
            setValidInput(false)
        } else {
            props.createCategory({category_name: name, inventory_code: inventoryCode})
            setVisible(false)
            setName("")
            setInventoryCode("")
        }
    }

    return (
        <>
            <i className="pi p-pt-1 p-pl-3 pi-plus" style={{'fontSize': '1.7em', 'cursor': 'pointer'}}
               onClick={() => {
                   setVisible(true);
               }}/>
            <Dialog visible={visible} header="Kategorie erstellen" onHide={() => setVisible(false)}>
                <div className="p-d-flex p-flex-column">
                    <InputText className={inputClass} placeholder="Name" value={name} onChange={(e) => {
                        const input = e.target.value;
                        if (input.length > 0) {
                            setValidInput(true)
                        }
                        setName(input);
                    }}/>
                    <InputText placeholder="Inventory Code" value={inventoryCode}
                               onChange={(e) => setInventoryCode(e.target.value)}/>
                    <span className="p-inputgroup-addon" style={{'cursor': 'pointer'}} onClick={saveClicked}>
                    <i className="pi pi-save"/>
                </span>
                </div>
            </Dialog>
        </>
    )
}

CategoryCreator.propTypes = {
    createCategory: PropTypes.func.isRequired
}
