import React from "react";
import PropTypes from 'prop-types';
import {AutoComplete} from "primereact/autocomplete";

export class AutoCompleteEditor extends React.Component {

    constructor(props) {
        super(props);

        this.filterItems = this.filterItems.bind(this);
        this.onChange = this.onChange.bind(this);

        this.state = {
            suggestions: []
        }
    }

    filterItems(event) {
        let suggestions;
        if (!event.query.trim().length) {
            suggestions = [...this.props.suggestions];
        } else {
            suggestions = this.props.suggestions.filter((element) => {
                return element.name.toLowerCase().startsWith(event.query.toLowerCase());
            });
            if (!suggestions.some(e => e.name === event.query)) {
                suggestions.push({
                    id: -1,
                    name: event.query
                });
            }
        }
        this.setState({suggestions});
    }

    onChange(event) {
        this.props.onChange(this.props.element, event.value)
    }

    render() {
        const values = this.props.values;
        const suggestions = this.state.suggestions;
        const field = this.props.field;
        return (
            <AutoComplete multiple id={field} field={'name'} style={{width: "100%"}} value={values}
                          suggestions={suggestions} completeMethod={this.filterItems}
                          onChange={this.onChange}/>
        )
    }
}

AutoCompleteEditor.propTypes = {
    element: PropTypes.shape.isRequired,
    values: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    field: PropTypes.string.isRequired,
    suggestions: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    onChange: PropTypes.func.isRequired
}