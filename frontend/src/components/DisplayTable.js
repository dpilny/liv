import {DataTable} from "primereact/datatable";
import PropTypes from 'prop-types';
import {Column} from "primereact/column";
import React from "react";
import {InputText} from "primereact/inputtext";
import {InputNumber} from "primereact/inputnumber";
import {isDesktop, isMobile} from "react-device-detect";
import {Dialog} from "primereact/dialog";
import {Button} from "primereact/button";
import {Dropdown} from "primereact/dropdown";
import {Chip} from "primereact/chip";
import {AutoCompleteEditor} from "./AutoCompleteEditor";

class DisplayTable extends React.Component {

    constructor(props) {
        super(props);
        this.originalRows = [];

        this.onEditorValueChange = this.onEditorValueChange.bind(this);
        this.inputTextEditor = this.inputTextEditor.bind(this);
        this.inputNumberEditor = this.inputNumberEditor.bind(this);
        this.dropdownEditor = this.dropdownEditor.bind(this);
        this.onRowEditInit = this.onRowEditInit.bind(this);
        this.onRowEditCancel = this.onRowEditCancel.bind(this);
        this.onRowEditSave = this.onRowEditSave.bind(this);

        this.onRowClicked = this.onRowClicked.bind(this);
        this.getFieldEditor = this.getFieldEditor.bind(this);
        this.getModalEditor = this.getModalEditor.bind(this);
        this.onSaveModal = this.onSaveModal.bind(this);
        this.onCancelModal = this.onCancelModal.bind(this);
        this.initEdit = this.initEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.saveEdit = this.saveEdit.bind(this);

        this.state = {
            editDialogVisible: false,
            selectedRow: -1
        }
    }

    onEditorValueChange(data, value, field, rowIndex) {
        let updatedData = [...data];
        updatedData[rowIndex][field] = value;
        this.props.updateData(updatedData);
    }

    inputTextEditor(data, value, field, rowIndex) {
        return <InputText id={field} style={{width: "100%"}} type="text" value={value}
                          onChange={(e) => this.onEditorValueChange(data, e.target.value, field, rowIndex)}/>;
    }

    inputNumberEditor(data, value, field, rowIndex) {
        return <InputNumber id={field} style={{width: "100%"}} type="number" mode="decimal" locale="de-DE"
                            value={value}
                            onValueChange={(e) => this.onEditorValueChange(data, e.target.value, field, rowIndex)}/>
    }

    inputChipsEditor(element, data, values, suggestions, field, onChange) {
        return <AutoCompleteEditor element={element} values={values} suggestions={suggestions} field={field} onChange={onChange}/>
    }

    chipDisplay(column) {
        return function (rowData) {
            let elements;
            if (column.chipValuesFunc) {
                elements = column.chipValuesFunc(rowData);
            } else {
                elements = rowData[column.field]
            }
            return (
                <>
                    {elements.map((value, index) => {
                        return <Chip key={value.id} label={value.name}/>
                    })}
                </>
            )
        }
    }

    dropdownEditor(data, value, field, rowIndex, options) {
        if (isMobile) {
            return (
                <select className="form-select" value={value}
                        onChange={(e) => this.onEditorValueChange(data, e.target.value, field, rowIndex)}>
                    {options.map(function (option, i) {
                        return <option value={option.name} key={i}>{option.name}</option>;
                    })}
                </select>
            )
        } else {
            return (
                <Dropdown id={field} value={value} options={options} optionLabel="name"
                          optionValue="name"
                          filter={true}
                          onChange={(e) => this.onEditorValueChange(data, e.value, field, rowIndex)}
                          style={{width: '100%'}}
                          itemTemplate={(option) => {
                              return <span>{option.name}</span>
                          }}/>
            )
        }
    }

    initEdit(index) {
        this.originalRows[index] = {...this.props.data[index]};
    }

    cancelEdit(index) {
        let updatedData = [...this.props.data];
        updatedData[index] = this.originalRows[index];
        delete this.originalRows[index];
        this.props.updateData(updatedData);
    }

    saveEdit(index) {
        this.props.updateItem(this.originalRows[index], this.props.data[index]);
    }

    onRowEditInit(event) {
        this.initEdit(event.index)
    }

    onRowEditCancel(event) {
        this.cancelEdit(event.index)
    }

    onRowEditSave(event) {
        this.saveEdit(event.index)
    }

    onRowClicked(event) {
        if (isDesktop) {
            return
        }
        this.setState({editDialogVisible: true, selectedRow: event.index})
        this.initEdit(event.index)
    }

    onSaveModal() {
        this.saveEdit(this.state.selectedRow)
        this.setState({editDialogVisible: false, selectedRow: -1})
    }

    onCancelModal() {
        this.cancelEdit(this.state.selectedRow)
        this.setState({editDialogVisible: false, selectedRow: -1})
    }

    getFieldEditor(column) {
        switch (column.type) {
            case "string":
                return (editorProps) => this.inputTextEditor(editorProps.value, editorProps.rowData[column.field], column.field, editorProps.rowIndex)
            case "number":
                return (editorProps) => this.inputNumberEditor(editorProps.value, editorProps.rowData[column.field], column.field, editorProps.rowIndex)
            case "dropdown":
                return (editorProps) => this.dropdownEditor(editorProps.value, editorProps.rowData[column.field], column.field, editorProps.rowIndex, column.options)
            case "chips":
                return (editorProps) => {
                    let elements;
                    if (column.chipValuesFunc) {
                        elements = column.chipValuesFunc(editorProps.value[editorProps.rowIndex]);
                    } else {
                        elements = editorProps.value[editorProps.rowIndex][column.field]
                    }
                    return this.inputChipsEditor(editorProps.value[editorProps.rowIndex], editorProps.value, elements, column.suggestions, column.field, column.onChange)
                }
        }
        return null;
    }

    getModalEditor(column, data, rowIndex) {
        if (column.editable) {
            switch (column.type) {
                case "string":
                    return this.inputTextEditor(data, data[rowIndex][column.field], column.field, rowIndex)
                case "number":
                    return this.inputNumberEditor(data, data[rowIndex][column.field], column.field, rowIndex)
                case "dropdown":
                    return this.dropdownEditor(data, data[rowIndex][column.field], column.field, rowIndex, column.options)
            }
        }
        return null;
    }

    render() {
        let columns = this.props.columns.map((column) => {
            let editor;
            if (column.editable) {
                if (column.customEditor) {
                    editor = column.customEditor;
                } else {
                    editor = this.getFieldEditor(column);
                }
            }
            let body;
            if (column.type === 'chips') {
                body = this.chipDisplay(column);
            } else {
                body = column.body;
            }

            let style = {}
            if (isMobile && column.hideMobile) {
                style.display = 'none'
            }
            return <Column key={column.field} style={style} field={column.field} header={column.header} editor={editor}
                           body={body}
                           sortable={column.sortable}/>
        });

        let editMode = "";
        if (this.props.editable && isDesktop) {
            editMode = "row";
            columns.push(
                <Column key={"editor"} rowEditor headerStyle={{width: '7rem'}} bodyStyle={{textAlign: 'center'}}/>
            )
        }

        let editForm = [];
        if (isMobile) {
            for (let i = 0; i < this.props.columns.length; i++) {
                let col = this.props.columns[i];
                if (!col.editable || this.state.selectedRow < 0) {
                    continue
                }
                const editor = this.getModalEditor(col, this.props.data, this.state.selectedRow)
                editForm.push(
                    <div key={col.field} className="p-field">
                        <label htmlFor={col.field} className="p-sr-only">{col.field}</label>
                        {editor}
                    </div>
                )
            }
        }

        const footer = (
            <div>
                <Button label="Save" icon="pi pi-save" onClick={this.onSaveModal}/>
                <Button label="Cancel" icon="pi pi-times" onClick={this.onCancelModal}/>
            </div>
        );

        return (
            <>
                <DataTable className="liv-table" value={this.props.data} editMode={editMode} dataKey="id"
                           onRowEditInit={this.onRowEditInit} onRowClick={this.onRowClicked}
                           onRowEditCancel={this.onRowEditCancel} onRowEditSave={this.onRowEditSave}>
                    {columns}
                </DataTable>
                {isMobile &&
                <Dialog footer={footer} visible={this.state.editDialogVisible} style={{width: '100vw'}} modal
                        onHide={this.onCancelModal}>
                    <div className="p-formgroup">
                        {editForm}
                    </div>
                </Dialog>
                }
            </>
        )
    }
}

export default DisplayTable;

DisplayTable.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    updateData: PropTypes.func,
    columns: PropTypes.arrayOf(PropTypes.shape({
        field: PropTypes.string.isRequired,
        header: PropTypes.string.isRequired,
        customEditor: PropTypes.object,
        body: PropTypes.func,
        editable: PropTypes.bool,
        hideMobile: PropTypes.bool,
        type: PropTypes.oneOf(['string', 'number', 'dropdown', 'chips']),
        chipValuesFunc: PropTypes.func,
        options: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        })),
        sortable: PropTypes.bool,
        suggestions: PropTypes.array
    }).isRequired).isRequired,
    updateItem: PropTypes.func,
    editable: PropTypes.bool.isRequired
}