import React from "react";
import {Nav, Navbar} from "react-bootstrap";

function NavBar() {
    const path = window.location.pathname;
    return (
        <Navbar sticky="top" bg="dark" variant="dark" expand="lg" className="p-pl-2">
            <Navbar.Brand>LocalInventoryWatcher</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link active={path === "/"} href="/">Übersicht</Nav.Link>
                    <Nav.Link active={path === "/inventory"} href="/inventory">Inventar</Nav.Link>
                    <Nav.Link active={path === "/products"} href="/products">Produkte</Nav.Link>
                    <Nav.Link active={path === "/categories"} href="/categories">Kategorien</Nav.Link>
                    <Nav.Link active={path === "/lostnfound"} href="/lostnfound">Fundgrube</Nav.Link>
                    <Nav.Link active={path === "/log"} href="/log">Log</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavBar;