import React from "react";
import {Spinner} from "react-bootstrap";
import PropTypes from 'prop-types';

export default function ContentPage(props) {

    if (props.error) {
        return <div>Error: {props.error.message}</div>;
    } else if (!props.isLoaded) {
        return (<div className={"d-flex justify-content-center align-items-center"}>
            <Spinner className={''} animation={'border'} size={'xl'}/>
        </div>)
    } else {
        return (
            <>
                {props.children}
            </>
        )
    }

}

ContentPage.propTypes = {
    error: PropTypes.shape({
        message: PropTypes.string.isRequired
    }),
    isLoaded: PropTypes.bool.isRequired,
    children: PropTypes.array
}