function fetchJSON(url, params) {
    return fetch(url, params).then(result => {
        if (!result.ok) {
            throw Error(result.statusText);
        }
        return result.json();
    })
}

export function getProducts() {
    return fetchJSON("api/product");
}

export function getUnknownProducts() {
    return fetchJSON("api/product?type=unknown")
}

export function updateProduct(productId, payload) {
    return fetch("api/product/" + productId, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
}

export function getProductCategories() {
    return fetchJSON("api/product_category");
}

export function getProductCategoryTags() {
    return fetchJSON("api/product_category/tag");
}

export function updateProductCategory(categoryId, payload) {
    return fetch("api/product_category/" + categoryId, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
}

export function createProductCategory(categoryName, inventoryCode) {
    return fetch("api/product_category", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            category_name: categoryName,
            inventory_code: inventoryCode
        })
    })
}

export function getHistoryLog() {
    return fetchJSON("api/inventory/log");
}

export function getInventory() {
    return fetchJSON("api/inventory");
}

export function updateInventoryEntry(productId, payload) {
    return fetch("api/inventory/" + productId, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
}

export function getLocations() {
    return fetchJSON("api/location");
}