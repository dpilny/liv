create or replace table product_category
(
    id             int(11) unsigned auto_increment primary key,
    name           varchar(120) not null,
    inventory_code varchar(20)  null comment 'Code which used to identify this category for the barcode scanner.'
);

create table category_tag
(
    id int(11) unsigned auto_increment
        primary key,
    name varchar(45) not null
);

create table category_has_tag
(
    category_id int(11) unsigned not null,
    tag_id int(11) unsigned not null,
    constraint fk_category_has_tag_category
        foreign key (category_id) references product_category (id)
            on update cascade,
    constraint fk_category_has_tag_tag
        foreign key (tag_id) references category_tag (id)
            on update cascade,
    primary key (category_id, tag_id)
);

create or replace table location
(
    id   int(11) unsigned auto_increment primary key,
    name varchar(45) not null
);

create or replace table product
(
    id                  int(11) unsigned auto_increment primary key,
    name                varchar(255)     not null,
    description         varchar(512)     null,
    EAN                 varchar(20)      null,
    product_category_id int(11) unsigned null,
    constraint EAN
        unique (EAN),
    constraint fk_product_category_id
        foreign key (product_category_id) references product_category (id)
);

create or replace table inventory_entry
(
    product_id  int(11) unsigned           not null,
    location_id int(11) unsigned           not null,
    amount      int(11) unsigned default 1 not null,
    constraint inventory_entry_pk
        unique (product_id, location_id),
    constraint fk_inventory_location_id
        foreign key (location_id) references location (id),
    constraint fk_inventory_product_id
        foreign key (product_id) references product (id)
);

create or replace table history_log
(
    id         int(11) unsigned auto_increment primary key,
    action     varchar(25)                         not null,
    product_id int(11) unsigned                    not null,
    amount     int       default 1                 not null,
    timestamp  timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP
);

CREATE TRIGGER entry_add
    AFTER INSERT
    ON inventory_entry
    FOR EACH ROW
    INSERT INTO history_log (action, product_id, amount)
    VALUES ('add', NEW.product_id, NEW.amount);

DELIMITER //
CREATE TRIGGER entry_update
    AFTER UPDATE
    ON inventory_entry
    FOR EACH ROW
BEGIN
    DECLARE amountDif int;
    IF NEW.amount > OLD.amount THEN
        SET amountDif = NEW.amount - OLD.amount;
        INSERT INTO history_log (action, product_id, amount) VALUES ('add', NEW.product_id, amountDif);
    END IF;
    IF OLD.amount > NEW.amount THEN
        SET amountDif = OLD.amount - NEW.amount;
        INSERT INTO history_log (action, product_id, amount) VALUES ('remove', NEW.product_id, amountDif);
    END IF;
END;
//
