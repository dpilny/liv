INSERT INTO location (id, name) VALUES (1, 'Abstellkammer');

INSERT INTO product_category (id, name, inventory_code) VALUES (1, 'Zahnpasta', '#12345');
INSERT INTO product_category (id, name, inventory_code) VALUES (2, 'Haarwachs', null);
INSERT INTO product_category (id, name, inventory_code) VALUES (11, 'UNKNOWN', null);
INSERT INTO product_category (id, name, inventory_code) VALUES (12, 'Limonaden', null);
INSERT INTO product_category (id, name, inventory_code) VALUES (13, 'Mineralwasser', null);

INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (2, 'Got2B Strand Matte', 'Haarwachs Dennis', '4015100215618', 2);
INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (17, 'Mineralwasser, medium', 'Natürliches Mineralwasser mit wenig Kohlensäure', '4311596435968', 13);
INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (18, 'Schnellhefter', 'Grün', '4002372100414', 11);
INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (19, 'Mezzo Mix', 'Mezzo Mix 1l', '5449000017956', 12);
INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (20, 'Orangenlimonade', 'Sinalco - Orange Limonade', '4018715004196', 12);

INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (17, 1, 41);
INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (2, 1, 2);
INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (18, 1, 1);
INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (19, 1, 14);
INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (20, 1, 5);

INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (12, 'add', 20, 1, '2020-12-06 16:29:44');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (13, 'add', 20, 1, '2020-12-06 16:33:24');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (14, 'add', 20, 1, '2020-12-06 16:36:32');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (15, 'remove', 20, 4, '2020-12-06 16:37:50');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (16, 'add', 20, 5, '2020-12-06 16:38:03');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (17, 'remove', 20, 8, '2020-12-06 16:40:53');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (18, 'add', 17, 1, '2020-12-06 16:47:42');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (19, 'add', 17, 1, '2020-12-06 16:47:45');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (20, 'add', 17, 1, '2020-12-06 16:47:52');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (21, 'add', 17, 1, '2020-12-06 16:47:54');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (22, 'add', 17, 1, '2020-12-06 16:47:55');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (23, 'add', 17, 1, '2020-12-06 16:47:56');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (24, 'add', 17, 1, '2020-12-06 16:47:57');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (25, 'add', 17, 1, '2020-12-06 16:47:57');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (26, 'add', 17, 1, '2020-12-06 16:47:58');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (27, 'add', 17, 1, '2020-12-06 16:47:58');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (28, 'add', 17, 1, '2020-12-06 16:53:18');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (29, 'add', 17, 1, '2020-12-06 16:53:38');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (30, 'add', 17, 1, '2020-12-06 16:55:09');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (31, 'add', 17, 1, '2020-12-06 16:58:11');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (32, 'add', 17, 1, '2020-12-06 17:00:58');
INSERT INTO history_log (id, action, product_id, amount, timestamp) VALUES (33, 'add', 17, 1, '2020-12-06 17:01:05');