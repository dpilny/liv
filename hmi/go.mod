module gitlab.com/dpilny/liw/hmi

go 1.15

require (
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.6
	gitlab.com/dpilny/liw/grpc v0.0.0
	google.golang.org/grpc v1.33.2
)

replace gitlab.com/dpilny/liw/grpc => ../grpc
