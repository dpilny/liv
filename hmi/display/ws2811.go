package display

import (
	ws2811 "github.com/rpi-ws281x/rpi-ws281x-go"
	"log"
	"math"
	"sync"
	"time"
)

const (
	brightness = 200
)

type wsEngine interface {
	Init() error
	Render() error
	Wait() error
	Fini()
	Leds(channel int) []uint32
}

type LEDHandler struct {
	ws            wsEngine
	lastPermColor uint32
	isInBlink     bool
	jobQueue      chan displayJob
	mutex         sync.RWMutex
}

type displayJob struct {
	colors   []uint32
	duration int64
	interval int
}

func Get(ledCount int) (*LEDHandler, error) {
	opt := ws2811.DefaultOptions
	opt.Channels[0].Brightness = brightness
	opt.Channels[0].LedCount = ledCount

	device, err := ws2811.MakeWS2811(&opt)
	if err != nil {
		return nil, err
	}
	cw := &LEDHandler{
		ws: device,
		//blinkCtrlChan: make(chan bool),
		jobQueue: make(chan displayJob, 100),
	}
	err = cw.setup()
	if err != nil {
		return nil, err
	}

	cw.displayInitSequence()

	go cw.queueWorker()
	return cw, nil
}

func (cw *LEDHandler) setup() error {
	return cw.ws.Init()
}

func (cw *LEDHandler) EnqueueDisplayJob(colors []uint32, duration int64, interval int) bool {
	job := displayJob{
		colors:   colors,
		interval: interval,
		duration: duration,
	}
	log.Printf("enqueueing display job %+v\n", job)
	cw.clearBlink()
	select {
	case cw.jobQueue <- job:
		return true
	default:
		return false
	}
}

// displays the given colors in a blinking mode, times defines the amount of interval repetitions
// interval the time in ms at which each color is displayed
func (cw *LEDHandler) blink(colors []uint32, times, interval int) error {
	cw.writeIsInBlink(true)
	log.Printf("starting to blink with %v for %v times with a %v interval\n", colors, times, interval)
	for i := 0; i < times; i++ {
		for _, color := range colors {
			cw.mutex.RLock()
			if !cw.isInBlink {
				cw.mutex.RUnlock()
				log.Println("leaving blink mode")
				return nil
			}
			cw.mutex.RUnlock()
			err := cw.display(color)
			if err != nil {
				log.Println("finishing blink op due to error")
				cw.writeIsInBlink(false)
				return err
			}
			time.Sleep(time.Duration(interval) * time.Millisecond)
		}
	}
	cw.writeIsInBlink(false)
	return cw.display(cw.lastPermColor)
}

func (cw *LEDHandler) writeIsInBlink(val bool) {
	cw.mutex.Lock()
	cw.isInBlink = val
	cw.mutex.Unlock()
}

// display one color permanently
func (cw *LEDHandler) displayPerm(color uint32) error {
	log.Println("displaying permanent color", color)
	cw.lastPermColor = color
	return cw.display(color)
}

// display one color for the specified duration in ms
func (cw *LEDHandler) displayTimed(color uint32, duration int64) error {
	log.Printf("displaying timed color %v for %v\n", color, duration)
	err := cw.display(color)
	if err != nil {
		return err
	}
	time.Sleep(time.Duration(duration) * time.Millisecond)
	return cw.display(cw.lastPermColor)
}

// processing jobs in job queue - job processing is blocking - so if in blink mode blink has to be cleaered before
func (cw *LEDHandler) queueWorker() {
	for job := range cw.jobQueue {
		log.Printf("processing job from queue %+v\n", job)
		switch len(job.colors) {
		case 0:
			err := cw.displayPerm(0)
			if err != nil {
				log.Println("failed to clear led", err)
			}
		case 1:
			if job.duration == 0 {
				err := cw.displayPerm(job.colors[0])
				if err != nil {
					log.Println("failed to set perm color", err)
				}
			} else {
				err := cw.displayTimed(job.colors[0], job.duration)
				if err != nil {
					log.Println("failed to display timed color", err)
				}
			}
		default:
			times := calculateTimes(job.duration, len(job.colors), job.interval)
			err := cw.blink(job.colors, times, job.interval)
			if err != nil {
				log.Println("failed to blink")
			}
		}
	}
}

func calculateTimes(duration int64, colorCount, interval int) int {
	var times int
	if duration > 0 {
		times = int(duration) / (colorCount * interval)
		if times == 0 {
			times = 1
		}
	} else {
		times = math.MaxInt32
	}
	return times
}

// if the display is currently in blink mode clears it
func (cw *LEDHandler) clearBlink() {
	cw.mutex.Lock()
	if cw.isInBlink {
		log.Println("clearing previous blink")
		cw.isInBlink = false
		log.Println("cleared previous blink")
	}
	cw.mutex.Unlock()
}

func (cw *LEDHandler) displayInitSequence() {
	_ = cw.displayEachPixelTimed(uint32(0x0000ff))
	_ = cw.displayEachPixelTimed(uint32(0x00ff00))
	_ = cw.displayEachPixelTimed(uint32(0xff0000))
	_ = cw.displayEachPixelTimed(uint32(0x000000))
}

func (cw *LEDHandler) displayEachPixelTimed(color uint32) error {
	for i := 0; i < len(cw.ws.Leds(0)); i++ {
		cw.ws.Leds(0)[i] = color
		if err := cw.ws.Render(); err != nil {
			return err
		}
		time.Sleep(100 * time.Millisecond)
	}
	return nil
}

func (cw *LEDHandler) display(color uint32) error {
	for i := 0; i < len(cw.ws.Leds(0)); i++ {
		cw.ws.Leds(0)[i] = color
	}
	if err := cw.ws.Render(); err != nil {
		return err
	}
	return nil
}
