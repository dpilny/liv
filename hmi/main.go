package main

import (
	"fmt"
	"gitlab.com/dpilny/liw/hmi/server"
	"log"
)

func main() {
	srv, err := server.Get(":8080", 5)
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Println("starting grpc server")
	srv.Start()
	log.Println("finished server")
}
