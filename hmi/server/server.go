package server

import (
	"context"
	"gitlab.com/dpilny/liw/grpc/hmi"
	"gitlab.com/dpilny/liw/hmi/display"
	"google.golang.org/grpc"
	"log"
	"net"
)

type DisplayServer struct {
	address    string
	ledHandler *display.LEDHandler
}

type server struct {
	hmi.UnimplementedStateServiceServer
	ledHandler *display.LEDHandler
}

func (s *server) DisplayColor(ctx context.Context, request *hmi.HMIColorRequest) (*hmi.HMIColorReply, error) {
	log.Printf("got request with vals: %v\n", request.Colors)
	suc := s.ledHandler.EnqueueDisplayJob(request.Colors, request.Duration, int(request.Interval))
	log.Println("enqueued successfully:", suc)

	return &hmi.HMIColorReply{}, nil
}

func Get(address string, ledCount int) (*DisplayServer, error) {
	ledHandler, err := display.Get(ledCount)
	if err != nil {
		return nil, err
	}
	ds := &DisplayServer{
		address:    address,
		ledHandler: ledHandler,
	}
	return ds, nil
}

func (ds *DisplayServer) Start() {
	lis, err := net.Listen("tcp", ds.address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	hmi.RegisterStateServiceServer(s, &server{
		ledHandler: ds.ledHandler,
	})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
