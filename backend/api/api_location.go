package api

import (
	"encoding/json"
	"gitlab.com/dpilny/liw/backend/inventory"
	"log"
	"net/http"
)

func (a *API) getLocations(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")
	entries, err := a.LDB.GetLocations()
	if err != nil {
		// return code 500
		log.Println("failed to get locations", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []inventory.Location `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode entries", err)
		}
	}
}


