package api

import (
	"encoding/json"
	"gitlab.com/dpilny/liw/backend/product"
	"log"
	"net/http"
	"strconv"
)

type CreateCategoryTagRequest struct {
	TagName string `json:"tag_name"`
}

func (a *API) createCategoryTag(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	var rData CreateCategoryTagRequest
	if processRequestBody(w, r, &rData) {
		if rData.TagName == "" {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		_, err := a.LDB.CreateCategoryTag(rData.TagName)
		if err != nil {
			log.Println("failed to create category tag", rData, err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	}
}

func (a *API) addTagToCategory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
}

func (a *API) removeTagFromCategory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
}

func (a *API) getCategoryTags(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")

	var entries []product.CategoryTag
	var err error

	tagParam := r.URL.Query().Get("tagId")
	if tagParam != "" {
		categoryId, parseErr := strconv.Atoi(tagParam)
		if parseErr != nil {
			err = parseErr
		} else {
			entries, err = a.LDB.GetCategoryTags(categoryId)
		}
	} else {
		entries, err = a.LDB.GetAllCategoryTags()
	}

	if err != nil {
		log.Println("failed to get category tags", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []product.CategoryTag `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode category tag result", err)
		}
	}
}

