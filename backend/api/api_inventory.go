package api

import (
	"encoding/json"
	"gitlab.com/dpilny/liw/backend/inventory"
	"log"
	"net/http"
	"strconv"
)

func (a *API) getInventory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")
	entries, err := a.LDB.GetInventory()
	if err != nil {
		// return code 500
		log.Println("failed to getInventory", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []inventory.InventoryEntry `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode result", err)
		}
	}
}

type AddInventoryRequest struct {
	ProductId  int `json:"product_id"`
	LocationId int `json:"location_id"`
	Amount     int `json:"amount"`
}

func (a *API) addToInventory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var rData AddInventoryRequest
	if processRequestBody(w, r, &rData) {
		err := a.LDB.AddToInventory(rData.ProductId, rData.LocationId, rData.Amount)
		if err != nil {
			log.Println("failed to add to inventory", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusCreated)
		}
	}
}

func (a *API) deleteInventoryEntry(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	var (
		productId  int
		locationId int
	)

	productId, err := extractIdFromPath(r, "/api/inventory/")
	if err != nil {
		log.Println("failed to extract productId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	locationIdParam := r.URL.Query().Get("location_id")
	if locationIdParam == "" {
		log.Println("location_id for deleteInventoryEntry is not set")
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	locationId, err = strconv.Atoi(locationIdParam)

	err = a.LDB.RemoveFromInventory(productId, locationId)
	if err != nil {
		log.Println("failed to delete inventory entry:", productId, locationId, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

type UpdateInventoryRequest struct {
	LocationId    int  `json:"location_id"`
	NewAmount     *int `json:"new_amount,omitempty"`
	NewLocationId *int `json:"new_location_id,omitempty"`
}

func (a *API) updateInventoryEntry(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	productId, err := extractIdFromPath(r, "/api/inventory/")
	if err != nil {
		log.Println("failed to extract productId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var rData UpdateInventoryRequest
	if processRequestBody(w, r, &rData) {
		err := a.LDB.UpdateInventoryEntry(productId, rData.LocationId, rData.NewAmount, rData.NewLocationId)
		if err != nil {
			log.Println("failed to update inventory entry:", rData, err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusNoContent)
		}
	}
}

func (a *API) getInventoryLog(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")
	entries, err := a.LDB.GetInventoryLog()
	if err != nil {
		log.Println("failed to get inventory log", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []inventory.LogEntry `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode entries", err)
		}
	}
}

