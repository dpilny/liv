package api

import (
	"encoding/json"
	"gitlab.com/dpilny/liw/backend/inventory"
	"log"
	"net/http"
)

func (a *API) getProducts(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")

	var entries []inventory.ProductEntry
	var err error

	productType := r.URL.Query().Get("type")
	if productType == "unknown" {
		entries, err = a.LDB.GetUnknownProducts()
	} else {
		entries, err = a.LDB.GetAllProducts()
	}

	if err != nil {
		log.Println("failed to get products", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []inventory.ProductEntry `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode product result", err)
		}
	}
}

type UpdateProductRequest struct {
	ProductName        *string `json:"product_name,omitempty"`
	ProductDescription *string `json:"product_description,omitempty"`
	ProductCategory    *int    `json:"product_category,omitempty"`
	ProductCode        *string `json:"product_code,omitempty"`
}

func (a *API) updateProduct(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	productId, err := extractIdFromPath(r, "/api/product/")
	if err != nil {
		log.Println("failed to extract productId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var rData UpdateProductRequest
	if processRequestBody(w, r, &rData) {
		err = a.LDB.UpdateProduct(productId, rData.ProductName, rData.ProductDescription, rData.ProductCategory, rData.ProductCode)
		if err != nil {
			log.Println("failed to update product: ", rData, err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusNoContent)
		}
	}
}

func (a *API) removeProduct(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	productId, err := extractIdFromPath(r, "/api/product/")
	if err != nil {
		log.Println("failed to extract productId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	err = a.LDB.RemoveProduct(productId)
	if err != nil {
		log.Println("failed to delete product:", productId, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

