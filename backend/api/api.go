package api

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/dpilny/liw/backend/ldb"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type API struct {
	LDB *ldb.LiwDB
}

func (a *API) InitializeEndpoints(router *mux.Router) {
	// REST-Endpoints used to view an manipulate the users inventory
	router.HandleFunc("/inventory", a.getInventory).Methods("GET")
	router.HandleFunc("/inventory", a.addToInventory).Methods("POST")
	router.HandleFunc("/inventory/{id:[0-9]+}", a.updateInventoryEntry).Methods("PUT")
	router.HandleFunc("/inventory/{id:[0-9]+}", a.deleteInventoryEntry).Methods("DELETE")

	router.HandleFunc("/inventory/log", a.getInventoryLog).Methods("GET")

	// REST-Endpoints used to view and manipulate the product database
	router.HandleFunc("/product", a.getProducts).Methods("GET")
	router.HandleFunc("/product/{id:[0-9]+}", a.removeProduct).Methods("DELETE")
	router.HandleFunc("/product/{id:[0-9]+}", a.updateProduct).Methods("PUT")

	// REST-Endpoints used to view and manipulate the product_category database
	router.HandleFunc("/product_category", a.getProductCategories).Methods("GET")
	router.HandleFunc("/product_category", a.createProductCategory).Methods("POST")
	router.HandleFunc("/product_category/{id:[0-9]+}", a.updateProductCategory).Methods("PUT")
	router.HandleFunc("/product_category/{id:[0-9]+}", a.deleteProductCategory).Methods("DELETE")

	// REST-Endpoints used to view and manipulate the category_tag database
	router.HandleFunc("/product_category/tag", a.getCategoryTags).Methods("GET")
	router.HandleFunc("/product_category/tag", a.createCategoryTag).Methods("POST")
	router.HandleFunc("/product_category/{id:[0-9]+}/tag/{id:[0-9]+}", a.addTagToCategory).Methods("POST")
	router.HandleFunc("/product_category/{id:[0-9]+}/tag/{id:[0-9]+}", a.removeTagFromCategory).Methods("DELETE")

	router.HandleFunc("/location", a.getLocations).Methods("GET")
}

var InvalidPathError = errors.New("invalid path error")

func extractIdByIndex(path string, index int) (int, error) {
	parts := strings.Split(path, "/")
	if len(parts) >= index {
		return -1, InvalidPathError
	}
	return strconv.Atoi(parts[index])
}

func extractIdFromPath(r *http.Request, pathPrefix string) (int, error) {
	stringId := strings.TrimPrefix(r.URL.Path, pathPrefix)
	id, err := strconv.Atoi(stringId)
	if err != nil {
		return 0, err
	}
	return id, nil
}

// decodes request data from request body, checks for errors and writes error response if necessary
// returns true if decode was successful
func processRequestBody(w http.ResponseWriter, r *http.Request, rData interface{}) bool {
	err := decodeJSONBody(w, r, &rData)
	if err != nil {
		log.Println("invalid request: ", err)
		var mr *malformedRequest
		if errors.As(err, &mr) {
			http.Error(w, mr.msg, mr.status)
		} else {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return false
	}
	return true
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
