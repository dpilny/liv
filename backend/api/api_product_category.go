package api

import (
	"encoding/json"
	"gitlab.com/dpilny/liw/backend/product"
	"log"
	"net/http"
)

type UpdateProductCategoryRequest struct {
	CategoryName  string                 `json:"category_name"`
	InventoryCode string                 `json:"inventory_code"`
	Tags          *[]product.CategoryTag `json:"tags"`
}

type UpdatedTags struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func (a *API) updateProductCategory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	categoryId, err := extractIdFromPath(r, "/api/product_category/")
	if err != nil {
		log.Println("failed to extract categoryId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var rData UpdateProductCategoryRequest
	if processRequestBody(w, r, &rData) {
		err := a.LDB.UpdateProductCategory(categoryId, rData.CategoryName, rData.InventoryCode, rData.Tags)
		if err != nil {
			log.Println("failed to update product category:", rData, err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusNoContent)
		}
	}
}

func (a *API) deleteProductCategory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	categoryId, err := extractIdFromPath(r, "/api/product_category/")
	if err != nil {
		log.Println("failed to extract productId from path", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	err = a.LDB.DeleteProductCategory(categoryId)
	if err != nil {
		log.Println("failed to delete product category:", categoryId, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

func (a *API) getProductCategories(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Add("Content-Type", "application/json")
	entries, err := a.LDB.GetProductCategories()
	if err != nil {
		// return code 500
		log.Println("failed to get product categories", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
		result := struct {
			Entries []product.Category `json:"entries"`
		}{Entries: entries}
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			log.Println("failed to encode entries", err)
		}
	}
}

type CreateProductCategoryRequest struct {
	CategoryName  string `json:"category_name"`
	InventoryCode string `json:"inventory_code"`
}

func (a *API) createProductCategory(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

	var rData CreateProductCategoryRequest
	if processRequestBody(w, r, &rData) {
		if rData.CategoryName == "" {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}
		_, err := a.LDB.CreateProductCategory(rData.CategoryName, rData.InventoryCode)
		if err != nil {
			log.Println("failed to create product category:", rData, err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	}
}
