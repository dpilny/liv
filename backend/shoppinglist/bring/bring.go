// Bring package contains methods to manipulate bring shopping lists
package bring

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const apiBase = "https://api.getbring.com/rest/v2"

var client = &http.Client{}

type Bring struct {
	session
}

type session struct {
	Uuid          string
	AccessToken   string `json:"access_token"`
	RefreshToken  string `json:"refresh_token"`
	BringListUUID string
}

type itemEntry struct {
	Name          string
	Specification string
}

type shoppingList struct {
	Items       []itemEntry `json:"purchase"`
	RecentItems []itemEntry `json:"recently"`
}

type ShoppingLists struct {
	Lists []struct {
		Name string
		Uuid string `json:"listUuid"`
	}
}

func Get(email, password string) (*Bring, error) {
	bring, err := login(email, password)
	if err != nil {
		return nil, err
	} else {
		return &bring, nil
	}
}

// performs the login action to the Bring REST-API
func login(email, password string) (Bring, error) {
	data := url.Values{
		"email":    []string{email},
		"password": []string{password},
	}

	r, err := http.NewRequest(http.MethodPost, apiBase+"/bringauth", strings.NewReader(data.Encode()))
	if err != nil {
		return Bring{}, err
	}
	r.Header.Add("Host", "api.getbring.com")

	resp, err := client.Do(r)
	if err != nil {
		return Bring{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return Bring{}, errors.New(fmt.Sprintf("failed with statusCode %v", resp.StatusCode))
	}

	log.Println("logged in to bring")
	var sessionData session
	err = json.NewDecoder(resp.Body).Decode(&sessionData)
	if err != nil {
		return Bring{}, err
	}
	return Bring{
		sessionData,
	}, nil
}

// Removes the given item from a bring list
func (b *Bring) RemoveItem(itemName, listId string) error {
	err := b.modifyList("recently", itemName, "", listId)
	if err != nil {
		return err
	}
	list, err := b.getListItems(listId)
	if err != nil {
		return err
	}
	if !containsItem(itemName, list.RecentItems) {
		return errors.New(fmt.Sprintf("failed to remove item %v from list %v", itemName, listId))
	}
	return nil
}

func (b *Bring) AddItem(itemName, specification string) error {
	return b.AddItemToList(itemName, specification, "")
}

// Add an item to a bring list with an optional specification
// if no listId is provided the sessions "BringListUUID" is taken
func (b *Bring) AddItemToList(itemName, specification, listId string) error {
	if listId == "" {
		listId = b.session.BringListUUID
	}
	err := b.modifyList("purchase", itemName, specification, listId)
	if err != nil {
		return err
	}
	list, err := b.getListItems(listId)
	if err != nil {
		return err
	}
	if !containsItem(itemName, list.Items) {
		return errors.New(fmt.Sprintf("failed to add item %v to list %v", itemName, listId))
	}
	return nil
}

func (b *Bring) modifyList(modifyType, itemName, specification, listId string) error {
	data := url.Values{
		modifyType:      []string{itemName},
		"specification": []string{specification},
	}

	r, err := http.NewRequest(http.MethodPut, apiBase+"/bringlists/"+listId, strings.NewReader(data.Encode()))
	if err != nil {
		return nil
	}
	b.addAuthHeaders(r)

	log.Println("modifying item", itemName, "to list", listId)

	resp, err := client.Do(r)
	if err != nil {
		return err
	}

	if resp.StatusCode != 204 {
		return errors.New(fmt.Sprintf("failed with statusCode %v", resp.Status))
	}

	log.Print("status: ", resp.Status)
	return nil
}

func (b *Bring) getListItems(listId string) (shoppingList, error) {
	r, err := http.NewRequest(http.MethodGet, apiBase+"/bringlists/"+listId, nil)
	if err != nil {
		return shoppingList{}, errors.New(fmt.Sprintf("failed to create shopping list request %v", err))
	}
	b.addAuthHeaders(r)

	resp, err := client.Do(r)
	if err != nil {
		return shoppingList{}, errors.New(fmt.Sprintf("failed to request shopping list %v", err))
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return shoppingList{}, errors.New(fmt.Sprintf("failed with statusCode %v", resp.StatusCode))
	}

	log.Println("status: ", resp.Status)
	log.Println("retrieved list for id", listId)
	var list shoppingList
	err = json.NewDecoder(resp.Body).Decode(&list)
	if err != nil {
		return shoppingList{}, errors.New(fmt.Sprintf("failed to decode shopping list %v", err))
	}
	return list, nil
}

func (b *Bring) ShoppingLists() (ShoppingLists, error) {
	r, err := http.NewRequest(http.MethodGet, apiBase+"/bringusers/"+b.Uuid+"/lists", nil)
	if err != nil {
		return ShoppingLists{}, errors.New(fmt.Sprintf("failed to create request %v", err))
	}
	b.addAuthHeaders(r)

	resp, err := client.Do(r)
	if err != nil {
		return ShoppingLists{}, errors.New(fmt.Sprintf("failed to perform getShoppingList request %v", err))
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return ShoppingLists{}, errors.New(fmt.Sprintf("failed with statusCode %v", resp.StatusCode))
	}

	var lists ShoppingLists
	err = json.NewDecoder(resp.Body).Decode(&lists)
	if err != nil {
		return ShoppingLists{}, errors.New(fmt.Sprintf("failed to decode getShoppingList result body %v", err))
	}
	return lists, nil
}

func (b *Bring) addAuthHeaders(r *http.Request) {
	r.Header.Add("Host", "api.getbring.com")
	r.Header.Add("Authorization", "Bearer "+b.AccessToken)
	r.Header.Add("X-BRING-API-KEY", "cof4Nc6D8saplXjE3h3HXqHH8m7VU2i1Gs0g85Sp")
}

func containsItem(itemName string, items []itemEntry) bool {
	for _, v := range items {
		if v.Name == itemName {
			return true
		}
	}
	return false
}
