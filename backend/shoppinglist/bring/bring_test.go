package bring

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"testing"
)

func loadEnv() {
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatal("failed to load env", err)
	}
}

func ExampleLogin() {
	// login with basic auth credentials
	b, err := Get("MAIL@DOMAIN.TLD", "PWD")

	if err != nil {
		log.Fatal("failed to login to bring")
	}

	// call functions to inspect/modify your shopping lists
	b.ShoppingLists()
}

func TestInvalidLogin(t *testing.T) {
	loadEnv()
	_, err := Get(os.Getenv("bringEmail"), "asdbasd")

	if err == nil {
		t.Fatal("login with invalid pwd was successful")
	}
	log.Println("login failed expectedly", err)
}

func TestListFunctions(t *testing.T) {
	loadEnv()
	b, err := Get(os.Getenv("bringEmail"), os.Getenv("bringPassword"))

	if err != nil {
		t.Fatal("failed to login to bring", err)
	}

	lists, err := b.ShoppingLists()

	if err != nil {
		t.Fatal("failed to retrieve shopping lists", err)
	}

	var testListId string
	for _, v := range lists.Lists {
		if v.Name == "Test" {
			testListId = v.Uuid
		}
	}

	err = b.AddItemToList("Kohlrabi", "", testListId)
	if err != nil {
		t.Fatal("failed to add item to list", err)
	}

	err = b.RemoveItem("Kohlrabi", testListId)
	if err != nil {
		t.Fatal("failed to remove item from list", err)
	}
}
