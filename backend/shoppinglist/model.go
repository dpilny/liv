package shoppinglist

type ListHandler interface {
	AddItem(name, description string) error
}
