package util

import (
	"log"
)

// Check whether the execution of the given function returns an error and prints it
// https://stackoverflow.com/questions/40397781/gometalinter-errcheck-returns-a-warning-on-deferring-a-func-which-returns-a-va
func Check(f func() error) {
	if err := f(); err != nil {
		log.Println("Received error:", err)
	}
}
