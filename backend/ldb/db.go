package ldb

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/dpilny/liw/backend/product"
)

type LiwDB struct {
	db *sql.DB

	productResolver product.Resolver
}

func Get(db *sql.DB, productResolver product.Resolver) *LiwDB {
	return &LiwDB{db: db, productResolver: productResolver}
}

type Statistics struct {
	ProductData   map[string]int `json:"product_data"`
	InventoryData map[string]int `json:"inventory_data"`
}

func (ldb *LiwDB) getStatistics() {

}
