package ldb

import (
	"gitlab.com/dpilny/liw/backend/inventory"
	"gitlab.com/dpilny/liw/backend/util"
)

func (ldb *LiwDB) GetLocations() ([]inventory.Location, error) {
	var entries []inventory.Location

	rows, err := ldb.db.Query("SELECT id, name FROM location")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	var (
		locationId int
		name       string
	)

	for rows.Next() {
		err := rows.Scan(&locationId, &name)
		if err != nil {
			return nil, err
		}
		entry := inventory.Location{
			Id:   locationId,
			Name: name,
		}
		entries = append(entries, entry)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}


