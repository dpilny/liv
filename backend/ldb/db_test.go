package ldb

import (
	"database/sql"
	"errors"
	"github.com/joho/godotenv"
	"gitlab.com/dpilny/liw/backend/inventory"
	"gitlab.com/dpilny/liw/backend/product"
	"log"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

func initDB() *sql.DB {
	db, err := sql.Open("mysql", os.Getenv("testDataSourceName"))
	if err != nil {
		log.Fatal(err)
	}
	truncateData(db)

	return db
}

func truncateData(db *sql.DB) {
	log.Println("truncating test database")
	tx, err := db.Begin()
	if err != nil {
		log.Fatal("failed to init transaction", err)
	}
	_, _ = tx.Exec("SET FOREIGN_KEY_CHECKS=0;")
	_, _ = tx.Exec("TRUNCATE TABLE location;")
	_, _ = tx.Exec("TRUNCATE TABLE product;")
	_, _ = tx.Exec("TRUNCATE TABLE product_category;")
	_, _ = tx.Exec("TRUNCATE TABLE inventory_entry;")
	_, _ = tx.Exec("TRUNCATE TABLE history_log;")
	_, _ = tx.Exec("TRUNCATE TABLE category_tag;")
	_, _ = tx.Exec("TRUNCATE TABLE category_has_tag;")
	_, _ = tx.Exec("SET FOREIGN_KEY_CHECKS=1;")
	tx.Commit()
}

func intPtr(x int) *int {
	return &x
}

func addSampleData(db *sql.DB) {
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (1, 'Zahnpasta', 'item_toothpaste');")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (2, 'Haarwachs', 'item_hair_wax')")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (3, 'Tomaten', 'item_tomatoes')")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (4, 'Butter', 'item_butter')")
	_, _ = db.Exec("INSERT INTO product_category (id, name) VALUES (5, 'UNKNOWN')")

	_, _ = db.Exec("INSERT INTO category_tag (id, name) VALUES (1, 'Drogerie')")
	_, _ = db.Exec("INSERT INTO category_tag (id, name) VALUES (2, 'Obst')")
	_, _ = db.Exec("INSERT INTO category_tag (id, name) VALUES (3, 'Gemüse')")
	_, _ = db.Exec("INSERT INTO category_tag (id, name) VALUES (4, 'Reinigungsmittel')")

	_, _ = db.Exec("INSERT INTO category_has_tag (category_id, tag_id) VALUES (1, 1)")
	_, _ = db.Exec("INSERT INTO category_has_tag (category_id, tag_id) VALUES (1, 4)")
	_, _ = db.Exec("INSERT INTO category_has_tag (category_id, tag_id) VALUES (2, 1)")
	_, _ = db.Exec("INSERT INTO category_has_tag (category_id, tag_id) VALUES (3, 3)")

	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (1, 'Oral B', 'Das Beste vom Schlechtesten', '0123123121', 1);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (2, 'Got2B', 'Matt Haarwachs', '1233213022', 2);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (3, 'Beste Tomaten', 'Homemade', null, 3);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (4, 'Deutsche Markenbutter', 'Butter dude', '12341233234', 4);")
	_, _ = db.Exec("INSERT INTO product (id, name, EAN, product_category_id) VALUES (5, 'UNKNOWN PRODUCT',  '12341234333234', 5);")
	_, _ = db.Exec("INSERT INTO product (id, name, EAN, product_category_id) VALUES (6, 'Vio Mineralwasser',  '346531345984', 5);")
	_, _ = db.Exec("INSERT INTO product (id, name, EAN, product_category_id) VALUES (7, 'Ketchup',  '2347443733', 5);")

	_, _ = db.Exec("INSERT INTO location (id, name) VALUES (1, 'Abstellkammer');")
	_, _ = db.Exec("INSERT INTO location (id, name) VALUES (2, 'Badezimmer');")

	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (1, 1, 5);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (2, 1, 1);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (3, 1, 1);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (5, 1, 1);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (6, 1, 1);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (7, 1, 1);")
}

func loadEnv() {
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatal("failed to load env", err)
	}
}

func TestLiwDB_GetInventoryLog(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)

	t.Run("Initial prefill", func(t *testing.T) {
		entries, err := ldb.GetInventoryLog()
		if err != nil {
			t.Fatal("should not fail getting inventory log", err)
		}

		var (
			addAmount    int
			removeAmount int
		)

		for _, entry := range entries {
			switch entry.Action {
			case "add":
				addAmount++
			case "remove":
				removeAmount++
			}
		}

		if addAmount != 6 {
			t.Fatal("should have six added log entries, found", addAmount, "entries")
		}
		if removeAmount != 0 {
			t.Fatal("should have no 'remove' log entries, found", removeAmount, "log entries")
		}
	})
}

func TestLiwDB_GetInventory(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	t.Run("Empty-DB", func(t *testing.T) {
		inv, err := ldb.GetInventory()
		if err != nil {
			t.Fatal("should not fail getting inventory", err)
		}
		inventoryCount := len(inv)
		if inventoryCount != 0 {
			t.Fatal("inventory should be empty, has", inventoryCount, "entries")
		}
	})
	addSampleData(db)

	t.Run("Filled-DB", func(t *testing.T) {
		inv, err := ldb.GetInventory()
		if err != nil {
			t.Fatal("should not fail getting inventory", err)
		}
		inventoryCount := len(inv)
		if inventoryCount != 6 {
			t.Fatal("inventory should have six entries, has", inventoryCount, "entries")
		}
	})
}

func TestLiwDB_DecrementFromInventoryGetProduct(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)

	tests := []struct {
		name           string
		productId      int
		ean            string
		locationId     int
		expectedErr    error
		expectedAmount int
	}{
		{
			name:        "Test decrement unknown product",
			ean:         "123asd2",
			locationId:  1,
			expectedErr: product.ErrNotFound,
		},
		{
			name:        "Test decrement no inventory entry",
			ean:         "12341233234",
			locationId:  1,
			expectedErr: inventory.ErrNotFound,
		},
		{
			name:           "Test decrement known product amount > 1",
			ean:            "0123123121",
			locationId:     1,
			productId:      1,
			expectedAmount: 4,
		},
		{
			name:           "Test decrement known product amount = 1",
			ean:            "1233213022",
			locationId:     1,
			productId:      2,
			expectedAmount: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			newAmount, err := ldb.DecrementFromInventoryByEan(tt.ean, tt.locationId)

			if tt.expectedErr != nil {
				if tt.expectedErr != err {
					t.Fatalf("expected err from decrement: %v but got: %v\n", tt.expectedErr, err)
				}
			} else {
				if tt.expectedAmount != newAmount {
					t.Fatalf("expected new amount: %v but got %v\n", tt.expectedAmount, newAmount)
				}
				if tt.expectedAmount == 0 {
					row := db.QueryRow("SELECT * FROM inventory_entry WHERE product_id = ? AND location_id = ?;", tt.productId, tt.locationId)
					err = row.Scan()
					if !errors.Is(err, sql.ErrNoRows) {
						t.Fatal("expected to not find a inventory_entry for the decremented id, got:", err)
					}
				}
			}
		})
	}
}

func TestLiwDB_ResolveProduct(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)

	t.Run("Resolve unknown", func(t *testing.T) {
		_, err := ldb.ResolveProduct("")
		if !errors.Is(err, product.ErrNotFound) {
			t.Fatal("expected:", product.ErrNotFound, "'got:", err)
		}
	})

	t.Run("Resolve known", func(t *testing.T) {
		p, err := ldb.ResolveProduct("1233213022")
		if err != nil {
			t.Fatal("product should have been found but got:", err)
		}

		if p.ProductCategory != "Haarwachs" {
			t.Fatal("product category should be 'Haarwachs' but is:", p.ProductCategory)
		}

		if p.Name != "Got2B" {
			t.Fatal("product name should be 'Got2B' but is:", p.Name)
		}
	})
}

func TestLiwDB_CreateUnknownProduct(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)

	var unknownProductId int

	t.Run("Create first empty product", func(t *testing.T) {
		productId, err := ldb.createEmptyProduct("unknownEAN")

		if err != nil {
			t.Fatal("failed to create empty product")
		}
		unknownProductId = productId
	})

	t.Run("Call createEmptyProduct with the same input again", func(t *testing.T) {
		productId, err := ldb.createEmptyProduct("unknownEAN")

		if err != nil {
			t.Fatal("failed to retrieve already existing product")
		}
		if productId != unknownProductId {
			t.Fatal("retrieved empty product is is", productId, "but should be", unknownProductId)
		}
	})
}

func TestLiwDB_AddInventory(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)

	tests := []struct {
		name        string
		productId   int
		locationId  int
		expectedErr error
	}{
		{
			name:        "add new inventory entry",
			productId:   4,
			locationId:  1,
			expectedErr: nil,
		},
		{
			name:        "add entry with unknown productId",
			productId:   3567,
			locationId:  1,
			expectedErr: ErrInvalidLocationOrProductId,
		},
		{
			name:        "add entry with unknown locationId",
			productId:   4,
			locationId:  5,
			expectedErr: ErrInvalidLocationOrProductId,
		},
		{
			name:        "add entry which already is in inventory",
			productId:   4,
			locationId:  1,
			expectedErr: ErrEntryAlreadyExists,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ldb.AddToInventory(tt.productId, tt.locationId, 1)

			if tt.expectedErr != nil {
				if tt.expectedErr != err {
					t.Fatalf("expected err from decrement: %v but got: %v\n", tt.expectedErr, err)
				}
			} else {
				if err != nil {
					t.Fatalf("did not expect an err but got: %v\n", err)
				}
			}
		})
	}
}

func TestLiwDB_UpdateInventoryEntry(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	ldb := Get(db, nil)
	tests := []struct {
		name        string
		productId   int
		locationId  int
		newAmount   *int
		newLocation *int
	}{
		{
			name:        "Update amount (>0)",
			productId:   1,
			locationId:  1,
			newAmount:   intPtr(3),
			newLocation: nil,
		},
		{
			name:        "Update amount (=0)",
			productId:   1,
			locationId:  1,
			newAmount:   intPtr(0),
			newLocation: nil,
		},
		{
			name:        "Update location",
			productId:   2,
			locationId:  1,
			newAmount:   nil,
			newLocation: intPtr(2),
		},
		{
			name:        "Update location & amount",
			productId:   3,
			locationId:  1,
			newAmount:   intPtr(10),
			newLocation: intPtr(2),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ldb.UpdateInventoryEntry(tt.productId, tt.locationId, tt.newAmount, tt.newLocation)
			if err != nil {
				t.Fatal("did not expect to fail update inventory entry", err)
			}
			var amount int
			locationId := tt.locationId
			if tt.newLocation != nil {
				locationId = *tt.newLocation
			}
			err = db.QueryRow("SELECT amount FROM inventory_entry WHERE product_id = ? AND location_id = ?;", tt.productId, locationId).Scan(&amount)
			if tt.newAmount != nil {
				if *tt.newAmount == 0 {
					if err != sql.ErrNoRows {
						t.Fatal("entry should have been removed from inventory")
					}
				} else if *tt.newAmount != amount {
					t.Fatal("expected to have", *tt.newAmount, "but got", amount, "items in this entry")
				} else if err != nil {
					t.Fatal("unexpected error ", err)
				}
			}
			if tt.newLocation != nil && err != nil {
				if tt.newAmount != nil {
					if *tt.newAmount == 0 && err != sql.ErrNoRows {
						t.Fatal("failed to move inventory entry from", tt.locationId, "to", *tt.newLocation, err)
					}
				} else {
					t.Fatal("failed to move inventory entry from", tt.locationId, "to", *tt.newLocation, err)
				}
			}
		})
	}
}

func TestLiwDB_RemoveFromInventory(t *testing.T) {

}

func TestLiwDB_AddProductToInventoryByEan(t *testing.T) {

}

func TestLiwDB_DecrementFromInventoryByEan(t *testing.T) {

}

func TestLiwDB_AddToInventory(t *testing.T) {

}

func TestLiwDB_CreateProductCategory(t *testing.T) {

}

func TestLiwDB_DeleteProductCategory(t *testing.T) {

}

func TestLiwDB_GetProductCategories(t *testing.T) {

}

func TestLiwDB_GetCategoryNameByCode(t *testing.T) {

}

func TestLiwDB_UpdateProductCategory(t *testing.T) {

}

func TestLiwDB_GetAllProducts(t *testing.T) {

}

func TestLiwDB_RemoveProduct(t *testing.T) {

}

func TestLiwDB_HandleUnknownProduct(t *testing.T) {

}

func TestLiwDB_GetUnknownProducts(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	addSampleData(db)

	entries, err := ldb.GetUnknownProducts()
	if err != nil {
		t.Fatal("failed to fetch unknown products", err)
	}

	if len(entries) != 3 {
		t.Fatal("should have found three unknown products, but got:", len(entries))
	}
}

func TestLiwDB_UpdateProduct(t *testing.T) {

}

func TestLiwDB_GetLocations(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)

	t.Run("Empty-DB", func(t *testing.T) {
		locations, err := ldb.GetLocations()
		if err != nil {
			t.Fatal("should not fail getting inventory", err)
		}
		locationCount := len(locations)
		if locationCount != 0 {
			t.Fatal("locations should be empty, has", locationCount, "entries")
		}
	})
	addSampleData(db)

	t.Run("Filled-DB", func(t *testing.T) {
		locations, err := ldb.GetLocations()
		if err != nil {
			t.Fatal("should not fail getting inventory", err)
		}
		locationCount := len(locations)
		if locationCount != 2 {
			t.Fatal("locations should have two entries, has", locationCount, "entries")
		}
	})
}

func TestLiwDB_GetAllCategoryTags(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)

	t.Run("Empty-DB", func(t *testing.T) {
		tags, err := ldb.GetAllCategoryTags()
		if err != nil {
			t.Fatal("should not fail getting all category tags", err)
		}
		tagCount := len(tags)
		if tagCount != 0 {
			t.Fatal("tags should be empty, has", tagCount, "tags")
		}
	})
	addSampleData(db)

	t.Run("Filled-DB", func(t *testing.T) {
		tags, err := ldb.GetAllCategoryTags()
		if err != nil {
			t.Fatal("should not fail getting all category tags", err)
		}
		tagCount := len(tags)
		if tagCount != 4 {
			t.Fatal("tags should have four entries, has", tagCount, "tags")
		}
	})

}

func TestLiwDB_GetCategoryTags(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	addSampleData(db)

	tests := []struct {
		name             string
		categoryId       int
		expectedTagCount int
	}{
		{
			name:             "Multiple tags",
			categoryId:       1,
			expectedTagCount: 2,
		},
		{
			name:             "One Tag",
			categoryId:       2,
			expectedTagCount: 1,
		},
		{
			name:             "No Tags",
			categoryId:       4,
			expectedTagCount: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tags, err := ldb.GetCategoryTags(tt.categoryId)
			if err != nil {
				t.Fatal("failed to retrieve category tags", err)
			}
			tagCount := len(tags)
			if tagCount != tt.expectedTagCount {
				t.Fatal("expected to have", tt.expectedTagCount, "tags for category id", tt.categoryId, "but got", tagCount)
			}
		})
	}
}

func TestLiwDB_CreateCategoryTag(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	addSampleData(db)

	tests := []struct {
		name        string
		tagName     string
		expectedErr error
	}{
		{
			name:        "Create new",
			tagName:     "SomeTag",
			expectedErr: nil,
		},
		{
			name:        "Create already existing",
			tagName:     "Drogerie",
			expectedErr: CategoryTagAlreadyExists,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := ldb.CreateCategoryTag(tt.tagName)
			if err != tt.expectedErr {
				t.Fatalf("expected to get err: '%v' but got: '%v' err\n", tt.expectedErr, err)
			}
		})
	}
}

func TestLiwDB_AddCategoryTag(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	addSampleData(db)

	tests := []struct {
		name        string
		categoryId  int
		tagId       int
		expectedErr error
	}{
		{
			name:        "Add valid connection",
			categoryId:  1,
			tagId:       2,
			expectedErr: nil,
		},
		{
			name:        "Add already existing connection",
			categoryId:  1,
			tagId:       4,
			expectedErr: ConnectionAlreadyExists,
		},
		{
			name:        "Unknown category id",
			categoryId:  123,
			tagId:       2,
			expectedErr: InvalidTagConnection,
		},
		{
			name:        "Unknown tag id",
			categoryId:  2,
			tagId:       124,
			expectedErr: InvalidTagConnection,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ldb.AddTagToCategory(tt.categoryId, tt.tagId)
			if err != tt.expectedErr {
				t.Fatalf("expected to get err: '%v' but got: '%v' err\n", tt.expectedErr, err)
			}
		})
	}
}

func TestLiwDB_RemoveTagFromCategory(t *testing.T) {
	loadEnv()
	db := initDB()
	ldb := Get(db, nil)
	addSampleData(db)

	tests := []struct {
		name        string
		categoryId  int
		tagId       int
		expectedErr error
	}{
		{
			name:        "Remove valid connection",
			categoryId:  1,
			tagId:       1,
			expectedErr: nil,
		},
		{
			name:        "Connection did not exist",
			categoryId:  1,
			tagId:       2,
			expectedErr: InvalidTagConnection,
		},
		{
			name:        "Unknown category id",
			categoryId:  123,
			tagId:       2,
			expectedErr: InvalidTagConnection,
		},
		{
			name:        "Unknown tag id",
			categoryId:  2,
			tagId:       124,
			expectedErr: InvalidTagConnection,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ldb.RemoveTagFromCategory(tt.categoryId, tt.tagId)
			if err != tt.expectedErr {
				t.Fatalf("expected to get err: '%v' but got: '%v' err\n", tt.expectedErr, err)
			}
		})
	}
}
