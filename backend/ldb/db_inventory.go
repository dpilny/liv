package ldb

import (
	"database/sql"
	"errors"
	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/dpilny/liw/backend/inventory"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/util"
	"log"
	"time"
)

func (ldb *LiwDB) GetInventoryLog() ([]inventory.LogEntry, error) {
	var entries []inventory.LogEntry

	rows, err := ldb.db.Query("SELECT l.action, p.name, l.amount, l.timestamp FROM history_log l, product p WHERE l.product_id = p.id;")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	var (
		action      string
		productName string
		amount      int
		timestamp   time.Time
	)

	for rows.Next() {
		err := rows.Scan(&action, &productName, &amount, &timestamp)
		if err != nil {
			return nil, err
		}
		entry := inventory.LogEntry{
			Action:      action,
			ProductName: productName,
			Amount:      amount,
			Timestamp:   timestamp,
		}
		entries = append(entries, entry)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}

func (ldb *LiwDB) GetInventory() ([]inventory.InventoryEntry, error) {
	var entries []inventory.InventoryEntry

	rows, err := ldb.db.Query("SELECT p.name, p.description, p.ean, c.name as product_category, l.name as location, e.amount, p.id as product_id, l.id as location_id, c.id as product_category_id FROM inventory_entry e, product p, location l, product_category c WHERE p.id = e.product_id AND l.id = e.location_id AND p.product_category_id = c.id;")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	var (
		name              string
		description       sql.NullString
		category          string
		ean               sql.NullString
		location          string
		amount            int
		productId         int
		locationId        int
		productCategoryId int
	)

	for rows.Next() {
		err := rows.Scan(&name, &description, &ean, &category, &location, &amount, &productId, &locationId, &productCategoryId)
		if err != nil {
			return nil, err
		}
		entry := inventory.InventoryEntry{
			Product: product.Product{
				Id:              productId,
				Name:            name,
				Description:     description.String,
				Code:            ean.String,
				ProductCategory: category,
			},
			Amount:   amount,
			Location: location,
		}
		entries = append(entries, entry)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}

func (ldb *LiwDB) updateInventoryAmount(tx *sql.Tx, inventoryAmount, productId, locationId int) error {
	if inventoryAmount > 0 {
		_, err := tx.Exec("UPDATE inventory_entry SET amount = ? WHERE product_id = ? AND location_id = ?;", inventoryAmount, productId, locationId)
		if err != nil {
			return err
		}
	} else {
		_, err := tx.Exec("DELETE FROM inventory_entry WHERE product_id = ? AND location_id = ?;", productId, locationId)
		if err != nil {
			return err
		}
	}
	return nil
}

func (ldb *LiwDB) getInventoryAmount(tx *sql.Tx, productId, locationId int) (int, error) {
	var amount int
	err := tx.QueryRow("SELECT amount FROM inventory_entry WHERE product_id = ? AND location_id = ?;", productId, locationId).Scan(&amount)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return -1, inventory.ErrNotFound
		} else {
			return -1, err
		}
	}
	return amount, nil
}

func (ldb *LiwDB) AddProductToInventoryByEan(ean string, locationId int) error {
	tx, err := ldb.db.Begin()
	if err != nil {
		return err
	}
	var (
		productId int
	)

	log.Println("checking if product already exists in db")
	err = tx.QueryRow("SELECT p.id FROM product p WHERE ean = ?;", ean).Scan(&productId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			log.Println("unknown product for ean")
			productId, err = ldb.HandleUnknownProduct(ean)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}

	_, err = tx.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE amount = amount + ?;", productId, locationId, 1, 1)
	if err != nil {
		return err
	}

	return tx.Commit()
}

// DecrementFromInventoryByEan checks if for the given ean a product exists in the inventory, if so decrements the amount
// if the amount reaches zero, the inventory entry is removed
func (ldb *LiwDB) DecrementFromInventoryByEan(ean string, locationId int) (int, error) {
	tx, err := ldb.db.Begin()
	if err != nil {
		return -1, err
	}
	productId, err := ldb.checkIfProductExists(tx, ean)
	if err != nil {
		txErr := tx.Commit()
		if txErr != nil {
			return -1, txErr
		}
		return -1, err
	}

	inventoryAmount, err := ldb.getInventoryAmount(tx, productId, locationId)
	if err != nil {
		txErr := tx.Commit()
		if txErr != nil {
			return -1, txErr
		}
		return -1, err
	}
	inventoryAmount--
	err = ldb.updateInventoryAmount(tx, inventoryAmount, productId, locationId)
	txErr := tx.Commit()
	if txErr != nil {
		return -1, txErr
	}
	if err != nil {
		return -1, err
	}
	return inventoryAmount, nil
}

var ErrEntryAlreadyExists = errors.New("inventory entry already exists")

var ErrInvalidLocationOrProductId = errors.New("invalid location or product id")

// AddToInventory adds the given product to the specified location with the specified amount
// if the product or location is unknown ErrInvalidLocationOrProductId is returned, if the entry already exists ErrEntryAlreadyExists is returned
func (ldb *LiwDB) AddToInventory(productId int, locationId int, amount int) error {
	_, err := ldb.db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (?, ?, ?);", productId, locationId, amount)
	if err != nil {
		if sqlErr, ok := err.(*mysql.MySQLError); ok {
			if sqlErr.Number == mysqlerr.ER_NO_REFERENCED_ROW_2 {
				return ErrInvalidLocationOrProductId
			}
			if sqlErr.Number == mysqlerr.ER_DUP_ENTRY {
				return ErrEntryAlreadyExists
			}
		}
		return err
	}
	return nil
}

func (ldb *LiwDB) RemoveFromInventory(productId int, locationId int) error {
	_, err := ldb.db.Exec("DELETE FROM inventory_entry WHERE product_id = ? AND location_id = ?;", productId, locationId)
	if err != nil {
		return err
	}
	return nil
}

func (ldb *LiwDB) UpdateInventoryEntry(productId int, locationId int, newAmount *int, newLocationId *int) error {
	tx, err := ldb.db.Begin()
	if err != nil {
		return err
	}
	if newAmount != nil {
		if *newAmount <= 0 {
			_ = tx.Commit()
			return ldb.RemoveFromInventory(productId, locationId)
		} else {
			_, err := tx.Exec("UPDATE inventory_entry SET amount = ? WHERE product_id = ? AND location_id = ?;", *newAmount, productId, locationId)
			if err != nil {
				return err
			}
		}
	}

	if newLocationId != nil {
		var id int
		err := tx.QueryRow("SELECT id FROM location WHERE id = ?;", *newLocationId).Scan(&id)
		if err != nil {
			return err
		}

		_, err = tx.Exec("UPDATE inventory_entry SET location_id = ? WHERE product_id = ? AND location_id = ?;", *newLocationId, productId, locationId)

		if err != nil {
			return err
		}
	}
	return tx.Commit()
}


