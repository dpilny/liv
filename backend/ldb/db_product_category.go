package ldb

import (
	"database/sql"
	"errors"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/util"
	"strconv"
	"strings"
)

func (ldb *LiwDB) CreateProductCategory(categoryName string, inventoryCode string) (int64, error) {
	result, err := ldb.db.Exec("INSERT INTO product_category (name, inventory_code) VALUES (?,?);", categoryName, inventoryCode)
	if err != nil {
		return 0, err
	}
	catId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return catId, nil
}

func (ldb *LiwDB) UpdateProductCategory(categoryId int, categoryName, inventoryCode string, categoryTags *[]product.CategoryTag) error {
	if categoryName != "" {
		_, err := ldb.db.Exec("UPDATE product_category SET name = ? WHERE id = ?;", categoryName, categoryId)
		if err != nil {
			return err
		}
	}
	if inventoryCode != "" {
		_, err := ldb.db.Exec("UPDATE product_category SET inventory_code = ? WHERE id = ?;", inventoryCode, categoryId)
		if err != nil {
			return err
		}
	}
	if categoryTags != nil {
		err := ldb.updateCategoryTags(categoryId, categoryTags)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ldb *LiwDB) DeleteProductCategory(categoryId int) error {
	_, err := ldb.db.Exec("DELETE FROM product_category WHERE id = ?;", categoryId)
	return err
}

func (ldb *LiwDB) GetProductCategories() ([]product.Category, error) {
	var entries []product.Category

	rows, err := ldb.db.Query("SELECT pc.*, GROUP_CONCAT(cht.tag_id) as tag_ids FROM product_category pc LEFT JOIN category_has_tag cht on pc.id = cht.category_id group by pc.id;")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	var (
		categoryId    int
		name          string
		inventoryCode sql.NullString
		tagIdsSep     sql.NullString
	)

	for rows.Next() {
		var tagIds []int
		err = rows.Scan(&categoryId, &name, &inventoryCode, &tagIdsSep)
		if err != nil {
			return nil, err
		}

		if tagIdsSep.Valid {
			tagIds, err = convertStringToIntArray(strings.Split(tagIdsSep.String, ","))
			if err != nil {
				return nil, err
			}
		}

		entry := product.Category{
			Id:            categoryId,
			Name:          name,
			InventoryCode: inventoryCode.String,
			TagIds:        tagIds,
		}
		entries = append(entries, entry)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}

// move method out
func convertStringToIntArray(data []string) ([]int, error) {
	var values []int
	for _, value := range data {
		val, err := strconv.Atoi(value)
		if err != nil {
			return nil, err
		}
		values = append(values, val)
	}
	return values, nil
}

func (ldb *LiwDB) getUnknownProductCategory(tx *sql.Tx) (int64, error) {
	var unknownProductCatId int64
	err := tx.QueryRow("SELECT id FROM product_category WHERE name = ?;", "UNKNOWN").Scan(&unknownProductCatId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			r, err := tx.Exec("INSERT INTO product_category (name) VALUES ('UNKNOWN');")
			if err != nil {
				return 0, err
			}
			rowsAffected, err := r.RowsAffected()
			if err != nil {
				return 0, err
			}
			if rowsAffected == 1 {
				unknownProductCatId, err = r.LastInsertId()
				if err != nil {
					return 0, err
				}
			}
		} else {
			return 0, nil
		}
	}
	return unknownProductCatId, nil
}

// GetCategoryNameByCode returns the category name of a product identified by the code, category name is identified either through "inventory_code" from the product_category table or the products EAN
// if the code is neither a valid "inventory_code" nor the EAN of a product in the database returns "product.ErrNotFound"
func (ldb *LiwDB) GetCategoryNameByCode(code string) (string, error) {
	tx, err := ldb.db.Begin()
	if err != nil {
		return "", err
	}
	var (
		productCategory string
	)
	err = tx.QueryRow("SELECT c.Name as productCateroy FROM product_category c WHERE c.inventory_code = ?;", code).Scan(&productCategory)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			err = tx.QueryRow("SELECT c.name as productCategory FROM product p, product_category c WHERE p.ean = ? AND p.product_category_id = c.id;", code).Scan(&productCategory)
			if err != nil {
				if errors.Is(err, sql.ErrNoRows) {
					return "", product.ErrNotFound
				} else {
					return "", err
				}
			}
		}
	}
	return productCategory, nil
}

// retrieves the category id for a category name
func (ldb *LiwDB) getCategoryId(tx *sql.Tx, category string) (int64, error) {
	var categoryId int64
	err := tx.QueryRow("SELECT id FROM product_category WHERE name = ?;", category).Scan(&categoryId)
	return categoryId, err
}