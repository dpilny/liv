package ldb

import (
	"database/sql"
	"errors"
	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/util"
)

func (ldb *LiwDB) updateCategoryTags(categoryId int, categoryTags *[]product.CategoryTag) error {
	currentTags, err := ldb.GetCategoryTags(categoryId)
	currentTagIds := make(map[int]struct{})
	var Empty struct{}
	for _, tag := range currentTags {
		currentTagIds[tag.Id] = Empty
	}
	if err != nil {
		return err
	}
	for _, updatedTag := range *categoryTags {
		if updatedTag.Id == -1 {
			tagId, err := ldb.CreateCategoryTag(updatedTag.Name)
			if err != nil {
				return err
			}
			err = ldb.AddTagToCategory(categoryId, tagId)
			if err != nil {
				return err
			}
		} else {
			_, ok := currentTagIds[updatedTag.Id]
			if !ok {
				err = ldb.AddTagToCategory(categoryId, updatedTag.Id)
				if err != nil {
					return err
				}
			}
			delete(currentTagIds, updatedTag.Id)
		}
	}

	for tagId := range currentTagIds {
		err = ldb.RemoveTagFromCategory(categoryId, tagId)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ldb *LiwDB) GetCategoriesForTag(tagId int) ([]product.Category, error) {
	var entries []product.Category

	rows, err := ldb.db.Query("SELECT c.id, c.name, c.inventory_code FROM product_category AS c, category_has_tag ct WHERE c.id = ct.category_id AND ct.tag_id = ?;", tagId)
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	var (
		categoryId    int
		name          string
		inventoryCode sql.NullString
	)

	for rows.Next() {
		err = rows.Scan(&categoryId, &name, &inventoryCode)
		if err != nil {
			return nil, err
		}
		entry := product.Category{
			Id:            categoryId,
			Name:          name,
			InventoryCode: inventoryCode.String,
		}
		entries = append(entries, entry)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}


func (ldb *LiwDB) GetAllCategoryTags() ([]product.CategoryTag, error) {
	rows, err := ldb.db.Query("SELECT id, name FROM category_tag")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	return ldb.parseCategoryTagResults(rows)
}

func (ldb *LiwDB) GetCategoryTags(categoryId int) ([]product.CategoryTag, error) {
	rows, err := ldb.db.Query("SELECT c.id, c.name FROM category_tag c, category_has_tag ct WHERE ct.category_id = ? AND c.id = ct.tag_id", categoryId)
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)

	return ldb.parseCategoryTagResults(rows)
}

func (ldb *LiwDB) parseCategoryTagResults(rows *sql.Rows) ([]product.CategoryTag, error) {
	var entries []product.CategoryTag
	var (
		tagId int
		name  string
	)

	for rows.Next() {
		err := rows.Scan(&tagId, &name)
		if err != nil {
			return nil, err
		}
		entry := product.CategoryTag{
			Id:   tagId,
			Name: name,
		}
		entries = append(entries, entry)
	}
	err := rows.Err()
	if err != nil {
		return nil, err
	}

	return entries, nil
}

var CategoryTagAlreadyExists = errors.New("tag already exists")

func (ldb *LiwDB) CreateCategoryTag(tagName string) (int, error) {
	result, err := ldb.db.Exec("INSERT INTO category_tag (name) VALUES (?)", tagName)
	if sqlErr, ok := err.(*mysql.MySQLError); ok {
		if sqlErr.Number == mysqlerr.ER_DUP_ENTRY {
			return -1, CategoryTagAlreadyExists
		}
	}
	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	return int(id), err
}

var InvalidTagConnection = errors.New("invalid tag connection")

var ConnectionAlreadyExists = errors.New("tag already connected to category")

func (ldb *LiwDB) AddTagToCategory(categoryId, tagId int) error {
	_, err := ldb.db.Exec("INSERT INTO category_has_tag (category_id, tag_id) VALUES (?, ?)", categoryId, tagId)
	if sqlErr, ok := err.(*mysql.MySQLError); ok {
		if sqlErr.Number == mysqlerr.ER_DUP_ENTRY {
			return ConnectionAlreadyExists
		}
		if sqlErr.Number == mysqlerr.ER_NO_REFERENCED_ROW_2 {
			return InvalidTagConnection
		}
	}
	return err
}

func (ldb *LiwDB) RemoveTagFromCategory(categoryId, tagId int) error {
	result, err := ldb.db.Exec("DELETE FROM category_has_tag WHERE category_id = ? AND tag_id = ?", categoryId, tagId)
	if err != nil {
		return err
	}
	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if affectedRows == 0 {
		return InvalidTagConnection
	}
	return err
}
