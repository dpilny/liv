package ldb

import (
	"database/sql"
	"errors"
	"gitlab.com/dpilny/liw/backend/inventory"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/util"
	"log"
	"time"
)

func (ldb *LiwDB) GetUnknownProducts() ([]inventory.ProductEntry, error) {
	tx, err := ldb.db.Begin()
	if err != nil {
		return nil, err
	}
	unknownCategoryId, err := ldb.getUnknownProductCategory(tx)
	err = tx.Commit()
	if err != nil {
		return nil, err
	}
	rows, err := ldb.db.Query("SELECT p.id, p.name, p.description, p.EAN, c.name, l.timestamp "+
		"FROM product p, product_category c, history_log l "+
		"JOIN ("+
		"	SELECT product_id, MAX(timestamp) as timestamp "+
		"		FROM history_log li "+
		"		GROUP BY product_id "+
		" ) last_entry ON l.product_id = last_entry.product_id AND l.timestamp = last_entry.timestamp "+
		"WHERE p.id = l.product_id AND p.product_category_id = c.id AND c.id = ? "+
		"GROUP BY p.id;", unknownCategoryId)
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)
	return ldb.parseUnknownProductsResult(rows)
}

func (ldb *LiwDB) parseUnknownProductsResult(rows *sql.Rows) ([]inventory.ProductEntry, error) {
	var entries []inventory.ProductEntry

	var (
		id          int
		name        string
		description sql.NullString
		ean         sql.NullString
		category    string
		lastUpdate  time.Time
	)

	for rows.Next() {
		err := rows.Scan(&id, &name, &description, &ean, &category, &lastUpdate)
		if err != nil {
			return nil, err
		}
		entry := inventory.ProductEntry{
			Product: product.Product{
				Id:              id,
				Name:            name,
				Description:     description.String,
				ProductCategory: category,
				Code:            ean.String,
			},
			LastUpdate: lastUpdate,
		}

		entries = append(entries, entry)
	}
	err := rows.Err()
	if err != nil {
		return nil, err
	}
	return entries, nil
}

type UnknownProduct struct {
	product.Product
	lastScan time.Time
}

func (ldb *LiwDB) GetAllProducts() ([]inventory.ProductEntry, error) {
	rows, err := ldb.db.Query("SELECT p.id, p.name, p.description, p.ean, c.name as product_category FROM product p, product_category c WHERE p.product_category_id = c.id;")
	if err != nil {
		return nil, err
	}
	defer util.Check(rows.Close)
	return ldb.parseProductResult(rows)
}

func (ldb *LiwDB) parseProductResult(rows *sql.Rows) ([]inventory.ProductEntry, error) {
	var entries []inventory.ProductEntry

	var (
		id          int
		name        string
		description sql.NullString
		category    string
		ean         sql.NullString
	)

	for rows.Next() {
		err := rows.Scan(&id, &name, &description, &ean, &category)
		if err != nil {
			return nil, err
		}
		entry := inventory.ProductEntry{
			Product: product.Product{
				Id:              id,
				Name:            name,
				Description:     description.String,
				Code:            ean.String,
				ProductCategory: category,
			},
		}

		entries = append(entries, entry)
	}
	err := rows.Err()
	if err != nil {
		return nil, err
	}
	return entries, nil
}


func (ldb *LiwDB) UpdateProduct(productId int, productName *string, productDescription *string, categoryId *int, code *string) error {
	tx, err := ldb.db.Begin()
	if err != nil {
		return err
	}
	if productName != nil && *productName != "" {
		_, err := tx.Exec("UPDATE product SET name = ? WHERE id = ?;", *productName, productId)
		if err != nil {
			return err
		}
	}
	if productDescription != nil && *productDescription != "" {
		_, err := tx.Exec("UPDATE product SET description = ? WHERE id = ?;", *productDescription, productId)
		if err != nil {
			return err
		}
	}
	if code != nil && *code != "" {
		_, err := tx.Exec("UPDATE product SET EAN = ? WHERE id = ?;", &code, productId)
		if err != nil {
			return err
		}
	}
	if categoryId != nil {
		_, err := tx.Exec("UPDATE product SET product_category_id = ? WHERE id = ?;", *categoryId, productId)
		if err != nil {
			return err
		}
	}
	return tx.Commit()
}

func (ldb *LiwDB) RemoveProduct(productId int) error {
	tx, err := ldb.db.Begin()
	if err != nil {
		return err
	}
	rows, err := tx.Query("SELECT location_id FROM inventory_entry WHERE product_id = ?;", productId)
	if err != nil {
		return err
	}

	for rows.Next() {
		var locationId int
		err = rows.Scan(&locationId)
		if err != nil {
			return err
		}
		_, err = tx.Exec("DELETE FROM inventory_entry WHERE product_id = ? AND location_id = ?;", productId, locationId)
		if err != nil {
			return err
		}
	}

	_, err = tx.Exec("DELETE FROM product WHERE id = ?;", productId)

	return tx.Commit()
}
// ResolveProduct tries to resolve product from the database by the product EAN
func (ldb *LiwDB) ResolveProduct(ean string) (*product.Product, error) {
	log.Println("resolving product from ean:", ean)
	var (
		name        string
		description string
		category    string
	)

	err := ldb.db.QueryRow("SELECT p.name, p.description, c.name AS product_category FROM product p, product_category c WHERE p.ean = ? AND p.product_category_id = c.id;", ean).Scan(&name, &description, &category)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, product.ErrNotFound
		} else {
			return nil, err
		}
	}

	return &product.Product{
		Name:            name,
		Description:     description,
		Code:            ean,
		ProductCategory: category,
	}, nil
}

func (ldb *LiwDB) HandleUnknownProduct(ean string) (int, error) {
	var pr *product.Product
	var err error
	pr, err = ldb.productResolver.ResolveProduct(ean)
	if err != nil {
		log.Println("failed to resolve product", err)
	}
	if pr == nil {
		log.Println("creating empty product as resolver was not able to resolve the EAN")
		return ldb.createEmptyProduct(ean)
	} else {
		return ldb.insertNewProduct(pr)
	}
}

func (ldb *LiwDB) insertNewProduct(pr *product.Product) (int, error) {
	tx, err := ldb.db.Begin()
	if err != nil {
		return -1, err
	}
	var productCatId int64
	if pr.ProductCategory != "" {
		productCatId, err = ldb.getCategoryId(tx, pr.ProductCategory)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				productCatId, err = ldb.CreateProductCategory(pr.ProductCategory, "")
			} else {
				return 0, nil
			}
		}
	} else {
		productCatId, err = ldb.getUnknownProductCategory(tx)
		if err != nil {
			return 0, err
		}
	}
	id, err := ldb.insertProduct(tx, pr.Name, pr.Description, pr.Code, productCatId)
	txErr := tx.Commit()
	if txErr != nil {
		return -1, txErr
	}
	return id, err
}

func (ldb *LiwDB) checkIfProductExists(tx *sql.Tx, ean string) (int, error) {
	// check if product really does not exist
	rows, err := tx.Query("SELECT id FROM product WHERE EAN = ?;", ean)
	defer rows.Close()
	if err != nil {
		return -1, err
	}
	if rows.Next() {
		var productId int
		err = rows.Scan(&productId)
		if err != nil {
			return -1, err
		}
		return productId, nil
	}
	return -1, product.ErrNotFound
}

func (ldb *LiwDB) insertProduct(tx *sql.Tx, productName, description, ean string, categoryId int64) (int, error) {
	result, err := tx.Exec("INSERT INTO product (name, description, EAN, product_category_id) VALUES (?, ?, ?, ?);", productName, description, ean, categoryId)
	if err != nil {
		return -1, err
	}
	lastInsertId, err := result.LastInsertId()
	if err != nil {
		return -1, err
	}
	err = tx.Commit()
	if err != nil {
		return -1, err
	}
	return int(lastInsertId), nil
}

func (ldb *LiwDB) createEmptyProduct(ean string) (int, error) {
	tx, err := ldb.db.Begin()
	if err != nil {
		return -1, err
	}

	id, err := ldb.checkIfProductExists(tx, ean)
	// return found product id if error is not nil or return err if it wasn't a product.ErrNotFound
	if err == nil || err != product.ErrNotFound {
		txErr := tx.Commit()
		if txErr != nil {
			return -1, txErr
		}
		return id, err
	}

	// get UNKNOWN product category
	unknownProductCatId, err := ldb.getUnknownProductCategory(tx)
	if err != nil {
		log.Fatal("failed to retrieve unknown product category", err)
	}

	log.Println("unknown product category id:", unknownProductCatId)
	return ldb.insertProduct(tx, "UNKNOWN PRODUCT", "", ean, unknownProductCatId)
}
