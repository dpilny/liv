package product

import (
	"gitlab.com/dpilny/liw/backend/config"
	"log"
)

type Resolver interface {
	ResolveProduct(ean string) (*Product, error)
}

type MockResolver struct {
}

func (m *MockResolver) ResolveProduct(ean string) (*Product, error) {
	return &Product{
		Name: "Mockproduct",
		Code: ean,
	}, nil
}

type combinedResolver struct {
	config   *config.Config
	openGTIN openGTIN
}

func (c *combinedResolver) ResolveProduct(ean string) (*Product, error) {
	log.Println("trying to resolve product for ean:", ean)
	var pr *Product
	var err error
	if c.config.OpenGTINEnabled {
		log.Println("resolving product by openGTIN")
		pr, err = c.openGTIN.ResolveProduct(ean)
		log.Println("openGTIN resolve result: ", pr, err)
	}
	return pr, err
}

func GetResolver(config *config.Config) Resolver {
	return &combinedResolver{config: config, openGTIN: openGTIN{apiKey: config.OpenGTINAPIKey}}
}
