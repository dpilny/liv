package product

import (
	"log"
	"os"
	"testing"

	"github.com/joho/godotenv"
)

func loadEnv() {
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatal("failed to load env", err)
	}
}

func TestOpenGTIN_ResolveProduct(t *testing.T) {
	loadEnv()
	gtin := openGTIN{apiKey: os.Getenv("openGTINAPIKey")}
	pr, err := gtin.ResolveProduct("4388840103942") // 4388441055879
	if err != nil {
		t.Fatal("failed to resolve product from opengtin", err)
	}
	log.Println("got product:", pr)
}

func TestParseProducts(t *testing.T) {
	tests := []struct {
		name                string
		body                string
		expectedName        string
		expectedDescription string
		expectedCategory    string
		expectedErr         error
	}{
		{
			name: "Natürliches Mineralwasser",
			body: `error=0
---
name=Natürliches Mineralwasser
detailname=Bad Vilbeler RIED Quelle
vendor=H. Kroner GmbH & CO. KG
maincat=Getränke, Alkohol
subcat=
contents=19
pack=1
descr=Natürliches Mineralwasser mit Kohlensäure versetzt
origin=Deutschland
validated=25 %
---`,
			expectedName:        "Natürliches Mineralwasser",
			expectedDescription: "Bad Vilbeler RIED Quelle",
			expectedCategory:    "Getränke, Alkohol",
		},
		{
			name: "Lätta",
			body: `
error=0
---
asin=
name=
detailname=Lätta Extra Fit Buttermilch, 500 ml
vendor=Laetta
maincat=
subcat=
maincatnum=-1
subcatnum=
contents=
pack=
origin=Niederlande
descr=Extraportion Buttermilch Fettarmer Brotaufstrich Erfrischend in den Tag starten Erhältlich in der 500g Packung
name_en=
detailname_en=
descr_en=
validated=0 %
---`,
			expectedName:        "Lätta Extra Fit Buttermilch, 500 ml",
			expectedDescription: "Extraportion Buttermilch Fettarmer Brotaufstrich Erfrischend in den Tag starten Erhältlich in der 500g Packung",
			expectedCategory:    "",
			expectedErr:         nil,
		},
		{
			name: "Quote Reached err",
			body: `error=5
---
`,
			expectedErr: ErrGTINQuotaReached,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			pr, err := parseProduct("test_ean", test.body)
			if test.expectedErr != nil {
				if test.expectedErr != err {
					t.Fatalf("expected err: %v, got err: %v\n", test.expectedErr, err)
				}
			} else {
				if err != nil {
					t.Fatal("failed to parse product", err)
				}

				if test.expectedName != pr.Name {
					t.Fatalf("expected name: %v, got name: %v\n", test.expectedName, pr.Name)
				}

				if test.expectedDescription != pr.Description {
					t.Fatalf("expected description: %v, got description: %v\n", test.expectedDescription, pr.Description)
				}

				if test.expectedCategory != pr.ProductCategory {
					t.Fatalf("expected category: %v, got category: %v\n", test.expectedCategory, pr.ProductCategory)
				}
			}
		})
	}
}
