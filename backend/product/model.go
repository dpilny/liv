package product

import (
	"errors"
)

var ErrNotFound = errors.New("product not found")

type Product struct {
	Id              int    `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	Code            string `json:"code"`
	ProductCategory string `json:"product_category"`
}

type Category struct {
	Id            int    `json:"id"`
	Name          string `json:"name"`
	InventoryCode string `json:"inventory_code"`
	TagIds        []int  `json:"tag_ids"`
}

type CategoryTag struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
