package product

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var ErrGTINQuotaReached = errors.New("api quota reached")

var categories = [...][]string{
	{"Baby, Kind", "Ausstattung", "Babygetränke", "Babynahrung", "Gesundheit, Pflege", "Kleider, Textilien", "Spiele Lernen", "Wickel"},
	{"Backwaren", "Backmischungen", "Backzutaten", "Brotarten", "Dauerbackwaren, Zwieback", "Frischbackwaren", "Gebäck, Panettone", "Hefe", "Kuchen, Cakes", "Teig"},
	{"Brotaufstriche", "Honig", "Konfitüren, Marmeladen", "verschiedene"},
	{"Dessert, Nachtisch", "Creme", "Pudding", "Speiseeis"},
	{"Eier", "Eier"},
	{"Elektrisch", "Anschluss-und Verbrauchsmaterial", "Batterien", "Licht", "Netzteile, Ladegeräte"},
	{"Elektronische Artikel", "Computer", "Fotografie", "HIFI", "Massenspeichermedien", "Tablet / PDA", "TV, Fernsehen", "Telefon", "Uhren", "Video", "DVD", "BD (Blu-ray)"},
	{"Fertiggerichte", "Andere", "Asia Gerichte", "Bouillon, Brühe", "Fleischerzeugnisse", "Gericht, Menü", "Kartoffelprodukte", "Pasta", "Pizza", "Salat", "Sandwich", "Saucen", "Suppen", "Tiefgekühltes, Tiefkühlkost"},
	{"Fleisch, Fisch", "Fisch", "Fischkonserven", "Fleischkonserven", "Frischfleisch", "Geflügel", "Meeresfrüchte", "Trockenfleisch, Salami", "Wurstwaren"},
	{"Früchte, Obst", "Exotische Früchte", "Früchte, Obst", "Nüsse", "Obstkonserven", "Trockenfrüchte", "kandierte Früchte"},
	{"Gemüse", "Antipasti", "Essigkonserven", "Gemüse", "Gemüsekonserven", "Salat", "Trockengemüse"},
	{"Getränke, Alkohol", "Alcopops", "Bier", "Energy Drinks", "Frucht-und Gemüsesäfte", "Instantgetränke", "Kaffee", "Kakao,Schokoladen", "Limonaden", "Mineralwasser", "Sirup", "Spirituosen", "Tee", "Wein/Sekt/Champagner"},
	{"Haushalt, Büro", "Bücher allgemein", "Fachbücher", "Literatur", "Bügeln, Textilpflege", "Dekoration", "Essen", "Garten", "Kleider, Textilien", "Küche", "Küchen-, Haushaltgeräte", "Kurzwaren/Mercerie", "Papeterie", "Zeitungen, Zeitschriften allgemein", "Fachzeitungen, -zeitschriften", "Schreib- und Zeichengeräte"},
	{"Kochzutaten", "Backpulver", "Essig", "Frische Gewürze", "Gelatine", "Gewürze", "Mehl", "Öl, Fette", "Salz", "Senf, Mayonnaise, Püree, Cremen", "Stärkearten"},
	{"Konditorei, Zuckerwaren", "Kuchendekoration", "Marzipan", "Süßstoffe", "Zucker"},
	{"Kosmetische Mittel", "Badezusätze", "Gesichtspflege", "Haarpflege", "Körperpflege", "Make-up Artikel", "Monatshygiene", "Nagel, Fusspflege", "Parfüm", "Pflaster, Watte", "Rasierprodukte", "Schwangerschaftstest", "Sonnen-, Insektenschutz", "Toilettenartikel", "Verhütung", "Zahnpflege"},
	{"Milchprodukte", "Butter, Margarine", "Joghurt", "Käse", "Milch", "MilchgetrÃ¤nke", "Quark", "Rahm, Rahmprodukte"},
	{"Präparate", "Calcium", "Magnesium", "Medikamente", "Sonstige", "Vitamine", "kombinierte Präparate"},
	{"Raucherwaren", "Tabak", "Zigaretten", "Zigarren", "Zubehör"},
	{"Sojaprodukte", "Sojamilch", "Sojasaucen", "Tofu", "sonstiges"},
	{"Süsswaren, Snacks", "Bisquits, Kekse, Konfekt", "Bonbons", "Chips", "Energiespender", "Fruchtgummi", "Getreide, Schokoriegel, Waffeln", "Kaugummi", "Schokolade", "salzige Snacks"},
	{"Teigwaren, Getreideprodukte", "Frühstücksflocken", "Getreide", "Teigwaren"},
	{"Tierbedarf", "Hunde", "Katzen", "Nager", "Sonstige"},
	{"Waschen, Reinigen", "Abwaschen", "Boden- und Teppichreiniger", "Entkalker", "Entsorgen", "Fleckenreiniger", "Glas-und Festerreiniger", "Küchenreiniger", "Lufterfrischer", "PutzgerÃ¤te und Zubehör", "Putzmittel", "Schuhpflege", "Spezialreiniger", "WC- und Bad Reiniger", "Waschmittel", "Wohnzimmerreiniger"},
	{"Zusatzstoffe", "Zusatzstoffe"},
	{"Haus, Hof und Freizeit", "Baumaterialien", "Farbe", "Werkzeuge", "Pflanzenmittel", "Pflanzen", "Modellbau", "Sportgeräte", "Sonstige", "Spielzeug/Spiele", "Sanitärmaterial"},
}

type openGTIN struct {
	apiKey string
}

func (o *openGTIN) ResolveProduct(ean string) (*Product, error) {
	resp, err := http.Get(fmt.Sprintf("http://opengtindb.org/?ean=%v&cmd=query&queryid=%v", ean, o.apiKey))
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return parseProduct(ean, toUtf8(body))
}

func toUtf8(iso8859_1_buf []byte) string {
	buf := make([]rune, len(iso8859_1_buf))
	for i, b := range iso8859_1_buf {
		buf[i] = rune(b)
	}
	return string(buf)
}

func parseProduct(ean, resultSet string) (*Product, error) {
	var (
		productName        string
		productDescription string
		productCategory    string
		desc               string
		mainCat            string
		mainCatNum         string
		subCatNum          string
	)
	for _, v := range strings.Split(resultSet, "\n") {
		if v == "---" || v == "" {
			continue
		}
		pair := strings.Split(v, "=")
		if len(pair) != 2 {
			log.Println("invalid result pair", v)
			continue
		}
		key := pair[0]
		val := pair[1]

		switch key {
		case "error":
			switch val {
			case "0":
			// no error
			case "1":
				return nil, ErrNotFound
			case "5":
				return nil, ErrGTINQuotaReached
			default:
				return nil, errors.New(fmt.Sprintf("failed getProductInfo request: %v", val))
			}
		case "name":
			if productName == "" {
				productName = val
			}
		case "detailname":
			if productDescription == "" {
				productDescription = val
			}
		case "maincat":
			if mainCat == "" {
				mainCat = val
			}
		case "subcat":
			if productCategory == "" {
				productCategory = val
			}
		case "descr":
			if desc == "" {
				desc = val
			}
		case "maincatnum":
			if mainCatNum == "" {
				mainCatNum = val
			}
		case "subcatnum":
			if subCatNum == "" {
				subCatNum = val
			}
		}
	}

	if productName == "" && productDescription != "" {
		productName = productDescription
		productDescription = desc
	}

	if productCategory == "" && mainCatNum != "" {
		mainIndex, err := strconv.Atoi(mainCatNum)
		if err == nil && mainIndex > 0 {
			if mainIndex < len(categories) {
				if subCatNum != "" {
					subIndex, err := strconv.Atoi(subCatNum)
					if err == nil && subIndex > 0 {
						if subIndex+1 < len(categories[mainIndex]) {
							productCategory = categories[mainIndex][subIndex+1]
						} else {
							productCategory = categories[mainIndex][0]
						}
					}
				} else {
					productCategory = categories[mainIndex][0]
				}
			}
		}
	}

	if productCategory == "" && mainCat != "" {
		productCategory = mainCat
	}

	return &Product{
		Name:            productName,
		Description:     productDescription,
		Code:            ean,
		ProductCategory: productCategory,
	}, nil
}
