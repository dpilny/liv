package scanner

import (
	"errors"
	"gitlab.com/dpilny/liw/backend/inventory"
	"gitlab.com/dpilny/liw/backend/ldb"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/shoppinglist"
	"log"
)

type Handler struct {
	ldb         *ldb.LiwDB
	listHandler shoppinglist.ListHandler
}

func GetHandler(ldb *ldb.LiwDB, listHandler shoppinglist.ListHandler) *Handler {
	return &Handler{
		ldb:         ldb,
		listHandler: listHandler,
	}
}

func (h *Handler) handleAddToInventoryAction(code string, locationId int) error {
	log.Println("handling add to inventory event - code is:", code)

	err := h.ldb.AddProductToInventoryByEan(code, locationId)
	if err != nil {
		log.Println("failed to handle add to inventory action", err)
		return err
	}
	return nil
}

func (h *Handler) handleRemoveFromInventoryAction(code string, locationId int) error {
	log.Println("handling remove event - code is:", code)

	_, err := h.ldb.DecrementFromInventoryByEan(code, locationId)

	if err != nil {
		if errors.Is(err, product.ErrNotFound) {
			log.Println("failed to remove/decrement item from inventory - no product known for code", code)
		} else if errors.Is(err, inventory.ErrNotFound) {
			log.Println("failed to remove/decrement item from inventory - no inventory entry for code", code)
		} else {
			log.Println("failed to remove/decrement item from inventory for code", code, err)
		}
		return err
	}
	return nil
}

func (h *Handler) handleRemoveFromInventoryAddToListAction(code string, locationId int) error {
	_, err := h.ldb.DecrementFromInventoryByEan(code, locationId)
	if err != nil {
		if errors.Is(err, product.ErrNotFound) {
			// 1. case: product not found
			log.Println("product not found for code")
			return err
		} else if errors.Is(err, inventory.ErrNotFound) {
			// 2. case: product was not in inventory
			log.Println("found product was not in inventory, still adding to shopping list")
		} else {
			log.Println("failed to decrement product from inventory for code", code, err)
			return err
		}
	}

	cat, err := h.ldb.GetCategoryNameByCode(code)
	if err != nil {
		log.Printf("failed to retrieve category name for ean: %v, err: %v\n", code, err)
		return err
	}

	err = h.listHandler.AddItem(cat, "")
	if err != nil {
		log.Println("failed to add product to shopping list")
		return err
	} else {
		log.Println("successfully added product to shopping list")
		return nil
	}
}

// resolves the code (either a product category id or a product EAN) then adds the product category name to the shopping list
func (h *Handler) handleAddToListAction(code string) error {
	cat, err := h.ldb.GetCategoryNameByCode(code)
	if err != nil {
		log.Println("failed to resolve product category for code", code, err)
		return err
	}
	log.Println("adding product", cat, "to shopping list")
	err = h.listHandler.AddItem(cat, "")
	if err != nil {
		log.Println("failed to add product to shopping list")
		return err
	} else {
		log.Println("successfully added product to shopping list")
		return nil
	}
}
