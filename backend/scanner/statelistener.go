package scanner

import (
	"gitlab.com/dpilny/liw/grpc"
	"log"
	"time"
)

type GRPCStateListener struct {
	Client *grpc.DisplayClient

	stateQueue chan stateJob
}

type EmptyStateListener struct {
}

func (l *EmptyStateListener) StateUpdated(state State) {}

func GetHMIListener(address string) (*GRPCStateListener, error) {
	client, err := grpc.GetClient(address)
	if err != nil {
		return nil, err
	}
	jobChan := make(chan stateJob, 100)
	listener := GRPCStateListener{
		Client:     client,
		stateQueue: jobChan,
	}
	go listener.queueWorker()
	return &listener, nil
}

func (l *GRPCStateListener) StateUpdated(state State) {
	log.Printf("scanner state updated %v\n", state)
	switch state.To {

	case StateIdle:
		// idle - no display
		l.enqueue([]uint32{}, 0, 0)

	case StateError:
		// error - display permanent red
		l.enqueue([]uint32{0xff0000}, 0, 0)

	case StateWaitForCode:
		if state.From == StateWaitForCode && state.PrevAction == state.Action {
			// display success if prevState was also WaitForCode and Action hasn't changed (this indicates that a successful scan was performed)
			l.enqueue([]uint32{0x00ff00, 0x0}, 250, 2500)
		}
		// user needs to scan a code - blink in action color
		var colors []uint32
		switch state.Action {
		case ActionAddItem:
			// yellow
			colors = append(colors, 0xffff00, 0x0)
		case ActionRemoveItem:
			// orange
			colors = append(colors, 0xffa500, 0x0)
		case ActionRemoveItemAddToList:
			// orange / cyan
			colors = append(colors, 0xffa500, 0x00ffff)
		case ActionAddToList:
			// cyan
			colors = append(colors, 0x00ffff, 0x0)
		}
		// blink permanently
		l.enqueue(colors, 500, 0)
	}
}

// enqueue tries to enqueue a job to the given job channel. Returns true if
// the operation was successful, and false if enqueuing would not have been
// possible without blocking. Job is not enqueued in the latter case.
func (l GRPCStateListener) enqueue(colors []uint32, interval int32, duration int64) bool {
	job := stateJob{
		colors:   colors,
		interval: interval,
		duration: duration,
	}
	select {
	case l.stateQueue <- job:
		return true
	default:
		return false
	}
}

func (l GRPCStateListener) queueWorker() {
	for job := range l.stateQueue {
		l.Client.DisplayState(job.colors, job.interval, job.duration)
		time.Sleep(time.Duration(job.duration) * time.Millisecond)
	}
}

type stateJob struct {
	colors   []uint32
	interval int32
	duration int64
}
