// package scanner contains implementations for handling input events from a barcode&qr code scanner
package scanner

import (
	"errors"
	"log"
)

const (
	StateIdle        = "state_idle"
	StateWaitForCode = "state_wait_for_code"
	StateError       = "state_error"
)

const (
	ActionAddItem             = "action_add_item"
	ActionRemoveItem          = "action_remove_item"
	ActionRemoveItemAddToList = "action_remove_item_add_to_list"
	ActionAddToList           = "action_add_to_list"
	ActionStopScan            = "action_stop_scan"
)

var ErrInvalidAction = errors.New("invalid action scanned")

// ActionHandler handles actions triggered by the scanner automaton
type ActionHandler interface {
	handleAddToInventoryAction(code string, locationId int) error

	handleAddToListAction(code string) error

	handleRemoveFromInventoryAction(code string, locationId int) error

	handleRemoveFromInventoryAddToListAction(code string, locationId int) error
}

type Scanner struct {
	PreviousState  string
	State          string
	PreviousAction string
	Action         string
	locationId     int
	handler        ActionHandler
	listener       StateListener
}

// listener which is called after each scan event and may be used to display state information to the user
type StateListener interface {
	StateUpdated(state State)
}

type State struct {
	From       string
	To         string
	PrevAction string
	Action     string
	Error      error
}

func Get(handler ActionHandler, listener StateListener) *Scanner {
	return &Scanner{
		State:    StateIdle,
		handler:  handler,
		listener: listener,
		// default locationId - currently locationId change is not supported
		locationId: 1,
	}
}

func (s *Scanner) Start(deviceId string) {
	ScanForever(deviceId, func(value string) {
		err := s.handleScannerEvent(value)
		s.listener.StateUpdated(State{
			From:       s.PreviousState,
			To:         s.State,
			PrevAction: s.PreviousAction,
			Action:     s.Action,
			Error:      err,
		})
	}, func(err error) {
		log.Println("failed scanning - resetting to StateError", err)
		s.State = StateError
		s.listener.StateUpdated(State{
			From:       s.PreviousState,
			To:         s.State,
			PrevAction: s.PreviousAction,
			Action:     s.Action,
			Error:      err,
		})
	})
}

func (s *Scanner) handleScannerEvent(code string) error {
	log.Println("scanned:", code, "in state:", s.State)
	switch s.State {
	case StateIdle, StateError:
		s.handleActionEvent(code)
	case StateWaitForCode:
		return s.handleWaitForCode(code)
	default:
		log.Fatal("Invalid scanner state: ", s.State)
		return ErrInvalidAction
	}
	return nil
}

func (s *Scanner) handleActionEvent(code string) {
	isAction := s.checkActionCode(code)

	if isAction {
		s.PreviousState = s.State
		s.State = StateWaitForCode
	}
}

func (s *Scanner) handleWaitForCode(code string) error {
	wasAction := s.checkActionCode(code)
	s.PreviousState = StateWaitForCode
	if wasAction {
		// no state change
		return nil
	} else if code == ActionStopScan {
		log.Println("stopping active scan")
		s.State = StateIdle
		s.Action = ""
		return nil
	} else {
		err := s.handleCodeAction(code)
		if err != nil {
			s.State = StateError
		}
		log.Println("handled '"+s.Action, "'")
		return err
	}
}

func (s *Scanner) handleCodeAction(code string) error {
	switch s.Action {
	case ActionAddItem:
		log.Println("handling add to inventory")
		return s.handler.handleAddToInventoryAction(code, s.locationId)
	case ActionAddToList:
		log.Println("handling add to list")
		return s.handler.handleAddToListAction(code)
	case ActionRemoveItem:
		log.Println("handling remove from inventory")
		return s.handler.handleRemoveFromInventoryAction(code, s.locationId)
	case ActionRemoveItemAddToList:
		log.Println("handling remove from inventory add to list")
		return s.handler.handleRemoveFromInventoryAddToListAction(code, s.locationId)
	}
	return nil
}

func (s *Scanner) checkActionCode(code string) bool {
	wasAction := false
	s.PreviousAction = s.Action
	switch code {
	case ActionAddItem:
		log.Println("setting action 'add item'")
		s.Action = ActionAddItem
		wasAction = true
	case ActionAddToList:
		log.Println("setting action 'add item to list'")
		s.Action = ActionAddToList
		wasAction = true
	case ActionRemoveItem:
		log.Println("setting action 'remove item'")
		s.Action = ActionRemoveItem
		wasAction = true
	case ActionRemoveItemAddToList:
		log.Println("setting action 'remove item and add to list'")
		s.Action = ActionRemoveItemAddToList
		wasAction = true
	}
	return wasAction
}
