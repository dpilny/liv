package scanner

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

// this test only works if the HMI gRPC-service is available
// there is also no way of checking if the queuing was successful as
func TestHMIQueuing(t *testing.T) {
	listener, err := GetHMIListener("192.168.178.55:8080")
	if err != nil {
		t.Fatalf("failed to init gprc connection %v", err)
		return
	}

	state := State{
		From:       "",
		To:         StateIdle,
		PrevAction: "",
		Action:     "",
		Error:      nil,
	}

	// expected result: no display
	fmt.Println("expected result: no display")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// go from idle to add item
	fmt.Println("go from idle to add item")
	state.From = StateIdle
	state.To = StateWaitForCode
	state.Action = ActionAddItem

	// expected result: display yellow blink
	fmt.Println("expected result: display yellow blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// scan a product
	fmt.Println("scan a product")
	state.From = StateWaitForCode
	state.PrevAction = ActionAddItem

	// expected result: display success for a bit, then go back to yellow blink
	fmt.Println("expected result: display success for a bit, then go back to yellow blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// change to add to list action
	fmt.Println("change to add to list action")
	state.Action = ActionAddToList

	// expected result: display cyan blink
	fmt.Println("expected result: display cyan blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// simulate some error
	fmt.Println("simulate some error")
	state.Error = errors.New("mockErr")
	state.To = StateError

	// expected result: display permanent red
	fmt.Println("expected result: display permanent red")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// go from error mode back to removeItem action
	fmt.Println("go from error mode back to removeItem action")
	state.Error = nil
	state.From = StateError
	state.To = StateWaitForCode
	state.Action = ActionRemoveItem

	// expected result: display orange blink
	fmt.Println("expected result: display orange blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// scan a product
	fmt.Println("scan a product")
	state.PrevAction = ActionRemoveItem
	state.From = StateWaitForCode

	// expected result: display success, then go back to orange blink
	fmt.Println("expected result: display success, then go back to orange blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// go from removeItem to removeItemAddToList action
	fmt.Println("go from removeItem to removeItemAddToList action")
	state.PrevAction = ActionRemoveItem
	state.Action = ActionRemoveItemAddToList

	// expected result: display orange/cyan blink
	fmt.Println("expected result: display orange/cyan blink")
	listener.StateUpdated(state)
	time.Sleep(5000 * time.Millisecond)

	// go back to idle
	fmt.Println("go back to idle")
	state.To = StateIdle

	// expected result: display none
	fmt.Println("expected result: display none")
	listener.StateUpdated(state)
	time.Sleep(500 * time.Millisecond)
}
