package scanner

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"gitlab.com/dpilny/liw/backend/ldb"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/shoppinglist/bring"
	"log"
	"os"
	"testing"
)

type MockStateListener struct {
}

func (l *MockStateListener) StateUpdated(state State) {
	log.Printf("state got updated: %v\n", state)
}

func initDB() *sql.DB {
	db, err := sql.Open("mysql", os.Getenv("testDataSourceName"))
	if err != nil {
		log.Fatal(err)
	}
	truncateData(db)

	return db
}

func truncateData(db *sql.DB) {
	tx, err := db.Begin()
	if err != nil {
		log.Fatal("failed to init transaction", err)
	}
	_, _ = tx.Exec("SET FOREIGN_KEY_CHECKS=0")
	_, _ = tx.Exec("TRUNCATE TABLE inventory_entry")
	_, _ = tx.Exec("TRUNCATE TABLE location")
	_, _ = tx.Exec("TRUNCATE TABLE product")
	_, _ = tx.Exec("TRUNCATE TABLE product_category")
	_, _ = tx.Exec("SET FOREIGN_KEY_CHECKS=1")
	err = tx.Commit()
}

func addSampleData(db *sql.DB) {
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (1, 'Zahnpasta', 'item_toothpaste');")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (2, 'Haarwachs', 'item_hair_wax')")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (3, 'Tomaten', 'item_tomatoes')")
	_, _ = db.Exec("INSERT INTO product_category (id, name, inventory_code) VALUES (4, 'Butter', 'item_butter')")

	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (1, 'Oral B', 'Das Beste vom Schlechtesten', '0123123121', 1);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (2, 'Got2B', 'Matt Haarwachs', '1233213022', 2);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (3, 'Beste Tomaten', 'Homemade', null, 3);")
	_, _ = db.Exec("INSERT INTO product (id, name, description, EAN, product_category_id) VALUES (4, 'Deutsche Markenbutter', 'Butter dude', '12341233234', 4);")

	_, _ = db.Exec("INSERT INTO location (id, name) VALUES (1, 'Abstellkammer');")

	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (1, 1, 5);")
	_, _ = db.Exec("INSERT INTO inventory_entry (product_id, location_id, amount) VALUES (2, 1, 1);")
}

func loadEnv() {
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatal("failed to load env", err)
	}
}

func TestScannerAutomaton(t *testing.T) {
	loadEnv()
	db := initDB()
	addSampleData(db)
	homeDB := ldb.Get(db, &product.MockResolver{})
	br, err := bring.Get(os.Getenv("bringEmail"), os.Getenv("bringPassword"))
	if err != nil {
		t.Error("failed to init bring")
	}
	scanHandler := GetHandler(homeDB, br)
	listener := MockStateListener{}
	scan := Get(scanHandler, &listener)

	t.Run("add known item to inventory", func(t *testing.T) {
		productId := 4
		productEan := "12341233234"
		count := getInventoryEntryCount(productId, db, t)
		if count > 0 {
			t.Fatal("product should not be in inventory already")
		}

		check(scan.State, StateIdle, t)

		err := scan.handleScannerEvent(ActionAddItem)
		if err != nil {
			t.Fatal("scan action event should not result in err:", err)
		}
		check(scan.State, StateWaitForCode, t)
		check(scan.Action, ActionAddItem, t)

		err = scan.handleScannerEvent(productEan)
		if err != nil {
			t.Fatal("scan product ean event should not result in err:", err)
		}
		check(scan.State, StateWaitForCode, t)
		check(scan.Action, ActionAddItem, t)

		count = getInventoryEntryCount(productId, db, t)
		if count == 1 {
			t.Error("product should have been added to inventory after scanner event")
		}

		err = scan.handleScannerEvent(ActionStopScan)
		if err != nil {
			t.Fatal("scan action event should not result in err:", err)
		}
		check(scan.State, StateIdle, t)
		check(scan.Action, "", t)
	})
	t.Run("add unknown item to inventory", func(t *testing.T) {
		productEan := "new_ean_123124"
		contains := checkProductExists(productEan, db, t)
		if contains {
			t.Fatal("product should not exist yet")
		}

		check(scan.State, StateIdle, t)
		err := scan.handleScannerEvent(ActionAddItem)
		if err != nil {
			t.Fatal("scan product ean event should not result in err:", err)
		}
		check(scan.Action, ActionAddItem, t)
		check(scan.State, StateWaitForCode, t)

		err = scan.handleScannerEvent(productEan)
		if err != nil {
			t.Fatal("scan product ean event should not result in err:", err)
		}
		check(scan.State, StateWaitForCode, t)
		check(scan.Action, ActionAddItem, t)

		productId, err := getProductId(productEan, db)
		if err != nil {
			t.Fatal("failed to retrieve product id", err)
		}
		entryCount := getInventoryEntryCount(productId, db, t)
		if entryCount <= 0 {
			t.Fatal("product should have been inserted to inventory")
		}

		err = scan.handleScannerEvent(ActionStopScan)
		if err != nil {
			t.Fatal("scan action event should not result in err:", err)
		}
		check(scan.State, StateIdle, t)
		check(scan.Action, "", t)
	})

	t.Run("add known product to shopping list", func(t *testing.T) {
		check(scan.State, StateIdle, t)
		err := scan.handleScannerEvent(ActionAddToList)
		if err != nil {
			t.Fatal("scan product ean event should not result in err:", err)
		}

		check(scan.Action, ActionAddToList, t)
		check(scan.State, StateWaitForCode, t)

		// butter
		err = scan.handleScannerEvent("12341233234")
		if err != nil {
			t.Fatal("scan action should not result in err: ", err)
		}

		check(scan.Action, ActionAddToList, t)
		check(scan.State, StateWaitForCode, t)

		err = scan.handleScannerEvent(ActionStopScan)
		if err != nil {
			t.Fatal("scan action event should not result in err:", err)
		}
		check(scan.State, StateIdle, t)
		check(scan.Action, "", t)
	})
	t.Run("add product category to shopping list", func(t *testing.T) {

	})
	t.Run("add unknown product to shopping list", func(t *testing.T) {

	})
}

func getProductId(ean string, db *sql.DB) (int, error) {
	var productId int
	err := db.QueryRow("SELECT id FROM product WHERE ean = ?;", ean).Scan(&productId)
	if err != nil {
		return 0, err
	} else {
		return productId, nil
	}
}

func checkProductExists(ean string, db *sql.DB, t *testing.T) bool {
	productId, err := getProductId(ean, db)
	if err != nil {
		if err == sql.ErrNoRows {
			return false
		} else {
			t.Fatal("unexpected Error", err)
		}
	}
	return productId > 0
}

func getInventoryEntryCount(productId int, db *sql.DB, t *testing.T) int {
	var count int
	err := db.QueryRow("SELECT COUNT(1) FROM inventory_entry WHERE product_id = ?;", productId).Scan(&count)
	if err != nil {
		if err == sql.ErrNoRows {
			return -1
		} else {
			t.Fatal("unexpected Error", err)
		}
	}
	return count
}

func check(got, expected string, t *testing.T) {
	if expected != got {
		t.Fatal("scanner is in invalid state. expected:", expected, "is:", got)
	}
}
