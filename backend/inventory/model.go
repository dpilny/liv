package inventory

import (
	"errors"
	"gitlab.com/dpilny/liw/backend/product"
	"time"
)

var ErrNotFound = errors.New("inventory entry not found")

type Location struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type LogEntry struct {
	Action      string    `json:"action"`
	ProductName string    `json:"product_name"`
	Amount      int       `json:"amount"`
	Timestamp   time.Time `json:"timestamp"`
}

type ProductEntry struct {
	product.Product
	LastUpdate time.Time `json:"last_update"`
}

type InventoryEntry struct {
	product.Product
	Amount   int    `json:"amount"`
	Location string `json:"location"`
}
