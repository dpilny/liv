package app

import (
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/dpilny/liw/backend/api"
	"gitlab.com/dpilny/liw/backend/config"
	"gitlab.com/dpilny/liw/backend/ldb"
	"gitlab.com/dpilny/liw/backend/product"
	"gitlab.com/dpilny/liw/backend/scanner"
	"gitlab.com/dpilny/liw/backend/shoppinglist"
	"gitlab.com/dpilny/liw/backend/shoppinglist/bring"
	"log"
	"net/http"
	"os"
	"path"
	"sync"
	"time"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type App struct {
	Config      *config.Config
	Router      *mux.Router
	listHandler shoppinglist.ListHandler
	scanner     *scanner.Scanner
	API         *api.API
	LDB         *ldb.LiwDB
}

func indexHandler(entryPoint string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, entryPoint)
	}
}

func initLogger(executionDir string) {
	fmt.Printf("exec dir is: %v\n", executionDir)
	file, err := os.OpenFile(fmt.Sprintf("%v/liw-log", executionDir), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(file)
}

func (a *App) Initialize() {
	ex, err := os.Executable()
	if err != nil {
		log.Println("failed to get working dir")
		log.Fatal(err)
	}
	dir := path.Dir(ex)
	initLogger(dir)

	log.Println("##############")
	log.Println("booting up liw server")
	log.Println("##############")

	a.Config = config.Get(dir)

	a.initializeBring()
	a.initializeDatabase()

	a.Router = mux.NewRouter()

	a.initializeAPI()
	a.initializeFrontend(dir)
	a.initializeScanner()
}

func (a *App) initializeBring() {
	bringAPI, err := bring.Get(a.Config.BringEmail, a.Config.BringPassword)
	if err != nil {
		log.Fatal(err)
	}
	a.listHandler = bringAPI
}

// scanner requires ldb and bring
func (a *App) initializeScanner() {
	if a.Config.ScannerActive {
		handler := scanner.GetHandler(a.LDB, a.listHandler)
		var scannerListener scanner.StateListener
		if a.Config.HMIActive {
			log.Println("connecting to hmi server")
			var err error
			scannerListener, err = scanner.GetHMIListener(a.Config.HMIServiceAddress)
			if err != nil {
				log.Fatalf("failed to initialize hmi listener: %v", err)
			}
		} else {
			scannerListener = &scanner.EmptyStateListener{}
		}
		a.scanner = scanner.Get(handler, scannerListener)
	}
}

func (a *App) initializeDatabase() {
	var dataSource string
	if a.Config.DBPassword == "" {
		dataSource = fmt.Sprintf("%v@tcp(%v)/%v?parseTime=true", a.Config.DBUser, a.Config.DBHost, a.Config.DBName)
	} else {
		dataSource = fmt.Sprintf("%v:%v@tcp(%v)/%v?parseTime=true", a.Config.DBUser, a.Config.DBPassword, a.Config.DBHost, a.Config.DBName)
	}
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		log.Fatal(err)
	}

	resolver := product.GetResolver(a.Config)
	a.LDB = ldb.Get(db, resolver)
}

func (a *App) initializeFrontend(dir string) {
	a.Router.PathPrefix("/static").Handler(http.FileServer(http.Dir(fmt.Sprintf("%v/frontend/build/", dir))))
	a.Router.PathPrefix("/").HandlerFunc(indexHandler(fmt.Sprintf("%v/frontend/build/index.html", dir)))
}

// API requires ldb
func (a *App) initializeAPI() {
	a.API = &api.API{LDB: a.LDB}
	apiRouter := a.Router.PathPrefix("/api/").Subrouter()
	a.API.InitializeEndpoints(apiRouter)
}

func (a *App) Run() {
	log.Println("starting server with addr", a.Config.Address, "on port", a.Config.Port)

	var wg sync.WaitGroup

	groups := 0
	if a.Config.ScannerActive {
		groups = 2
	} else {
		groups = 1
	}
	wg.Add(groups)

	go func() {
		defer wg.Done()
		srv := &http.Server{
			Handler:      handlers.LoggingHandler(os.Stdout, a.Router),
			Addr:         a.Config.Address + ":" + a.Config.Port,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		}

		if a.Config.SSL {
			log.Println("serving with TLS")
			log.Fatal(srv.ListenAndServeTLS("server.crt", "server.key"))
		} else {
			log.Println("serving")
			log.Fatal(srv.ListenAndServe())
		}
	}()

	if a.Config.ScannerActive {
		go func() {
			defer wg.Done()
			log.Println("starting scanner")
			a.scanner.Start(a.Config.ScannerDeviceId)
		}()
	}

	wg.Wait()
}
