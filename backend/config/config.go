package config

import (
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"strconv"
)

const (
	DefaultPort    = "80"
	DefaultAddress = "0.0.0.0"
)

type Config struct {
	DBUser     string
	DBPassword string
	DBName     string
	DBHost     string

	Address string
	Port    string
	SSL     bool

	ScannerActive   bool
	ScannerDeviceId string

	BringEmail    string
	BringPassword string

	OpenGTINAPIKey  string
	OpenGTINEnabled bool

	HMIActive         bool
	HMIServiceAddress string
}

func Get(executionDir string) *Config {
	conf := Config{}

	err := godotenv.Load(fmt.Sprintf("%v/.env", executionDir))
	if err != nil {
		log.Println("failed to load .env file")
		log.Fatal(err)
	}

	scannerActive, err := strconv.ParseBool(os.Getenv("scannerActive"))
	if err != nil {
		scannerActive = false
	}
	flag.BoolVar(&conf.ScannerActive, "scannerActive", scannerActive, fmt.Sprint("If true, Barcodescanner will be active"))
	flag.StringVar(&conf.ScannerDeviceId, "scannerDeviceId", os.Getenv("scannerDeviceId"), fmt.Sprint("DeviceId of Barcodescanner"))

	flag.StringVar(&conf.DBUser, "dbUser", os.Getenv("dbUser"), fmt.Sprint("Username for db access"))
	flag.StringVar(&conf.DBPassword, "dbPassword", os.Getenv("dbPassword"), fmt.Sprint("Password for db access"))
	flag.StringVar(&conf.DBName, "dbName", os.Getenv("dbName"), fmt.Sprint("Database name"))
	flag.StringVar(&conf.DBHost, "dbHost", os.Getenv("dbHost"), fmt.Sprint("Database host url"))

	flag.StringVar(&conf.Address, "host", DefaultAddress, fmt.Sprintf("The hostname or IP address of the API server (defaults to '%s')", DefaultAddress))
	flag.StringVar(&conf.Port, "port", os.Getenv("serverPort"), fmt.Sprintf("The internal API server port (defaults to '%v')", DefaultPort))

	sslActive, err := strconv.ParseBool(os.Getenv("ssl"))
	if err != nil {
		sslActive = false
	}
	flag.BoolVar(&conf.SSL, "ssl", sslActive, fmt.Sprintf("SSL on/off (defaults to 'false'"))

	flag.StringVar(&conf.BringEmail, "bringEmail", os.Getenv("bringEmail"), fmt.Sprintf("Bring Email Address"))
	flag.StringVar(&conf.BringPassword, "bringPassword", os.Getenv("bringPassword"), fmt.Sprintf("Bring Password"))

	flag.StringVar(&conf.OpenGTINAPIKey, "openGTINAPIKey", os.Getenv("openGTINAPIKey"), "API-Key for OpenGTIN API")

	openGTINEnabled, err := strconv.ParseBool(os.Getenv("openGTINEnabled"))
	if err != nil {
		openGTINEnabled = false
	}
	flag.BoolVar(&conf.OpenGTINEnabled, "openGTINEnabled", openGTINEnabled, "if true OpenGTIN product resolver is used to resolve unknown product EANs")

	hmiActive, err := strconv.ParseBool(os.Getenv("hmiActive"))
	if err != nil {
		hmiActive = false
	}
	flag.BoolVar(&conf.HMIActive, "hmiActive", hmiActive, "if true HMI will be activated")
	flag.StringVar(&conf.HMIServiceAddress, "hmiServiceAddress", os.Getenv("hmiServiceAddress"), "")

	flag.Parse()
	return &conf
}
