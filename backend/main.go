package main

import (
	"gitlab.com/dpilny/liw/backend/app"
	"log"
)

func main() {
	a := app.App{}
	a.Initialize()

	log.Println("##############")
	log.Println("initialized liw")
	log.Println("##############")

	a.Run()

	log.Println("##############")
	log.Println("shutting down liw")
	log.Println("##############")
}


